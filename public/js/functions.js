$(document).ready(function(){

    $('#tabelaUsuarios').DataTable({
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                text: 'Excel',
                className: 'btn btn-primary',
                alignment: "center",
                pageSize: 'LEGAL'
            },{
                extend: 'pdf',
                text: 'PDF',
                className: 'btn btn-primary',
                alignment: "center",
                pageSize: 'LEGAL'
            },
        ],
          "language": {
                "lengthMenu": "Mostrando _MENU_ registros por página",
                "zeroRecords": "Nada encontrado",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "Nenhum registro disponível",
                "infoFiltered": "(filtrado de _MAX_ registros no total)",
                "search": "Buscar:",
                "paginate": {
                        "previous": "Anterior",
                        "next": "Próxima"
                }
            },
           "ajax": {
            url : "usuarios/usuariosAtivos",
            type : 'GET'
        }
    });

    $('#tabelaUsuariosInativos').DataTable({
        dom: 'Bfrtip',
        buttons: [
             {
                extend: 'excel',
                text: 'Excel',
                className: 'btn btn-primary',
                alignment: "center",
                pageSize: 'LEGAL'
            },{
                extend: 'pdf',
                text: 'PDF',
                className: 'btn btn-primary',
                alignment: "center",
                pageSize: 'LEGAL'
            },
        ],
            "language": {
                  "lengthMenu": "Mostrando _MENU_ registros por página",
                  "zeroRecords": "Nada encontrado",
                  "info": "Mostrando página _PAGE_ de _PAGES_",
                  "infoEmpty": "Nenhum registro disponível",
                  "infoFiltered": "(filtrado de _MAX_ registros no total)",
                  "search": "Buscar:",
                  "paginate": {
                      "previous": "Anterior",
                      "next": "Próxima"
                   }
              },
             "ajax": {
              url : "usuarios/usuariosInativos",
              type : 'GET'
          }
      });
      
      
   $(function() {
       function output(state, value) {
           $('#expando-value').text(value);
//           console.log($('#visita_promotor').val())
       }
       
       $('#visita_promotor').tristate({
           change: output
       });
       $('#ficha_contato').tristate({
           change: output
       });
       $('#personalizado').tristate({
           change: output
       });
       $('#marca_propria').tristate({
           change: output
       });
       $('#com_celular').tristate({
           change: output
       });
   });

});