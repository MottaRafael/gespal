<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Clientes extends CI_Controller {

    function __construct(){
        
        parent::__construct();
        $logado = $this->session->userdata("logado");
	if ($logado != 1) redirect(base_url('admin/login'));	
        
    }

    function index() {
        
        $dados['titulo'] = "Clientes";
        $dados['subtitulo'] = "Filtrar";
        $dados['url'] = 'admin/relatorios/clientes';
        
        $dados['grupos_clifor'] = $this ->db->select('COD_GRUPO, DESCRICAO')
                                        ->from('GRUPOS_CLIFOR')
                                        ->order_by('DESCRICAO', 'ASC')
                                        ->where('COD_GRUPO !=', 1)
                                        ->where('COD_GRUPO !=', 6)
                                        ->get()
                                        ->result();
        
        $dados['representantes'] = $this ->db->select('CODIGO, NOME')
                                        ->from('REPRESENTANTES')
                                        ->where('ATIVO', 'S')
                                        ->order_by('NOME', 'ASC')
                                        ->get()
                                        ->result();
        
        $dados['vendedores'] = $this ->db->select('CODIGO, NOME')
                                        ->from('VENDEDORES')
                                        ->where('ATIVO', 'S')
                                        ->order_by('NOME', 'ASC')
                                        ->get()
                                        ->result();
        
        $dados['supervisores'] = $this ->db->select('CODIGO, NOME')
                                        ->from('SUPERVISORES')
                                        ->where('ATIVO', 'S')
                                        ->order_by('NOME', 'ASC')
                                        ->get()
                                        ->result();
        
        $dados['promotores'] = $this ->db->select('CODIGO, NOME')
                                        ->from('PROMOTORES')
                                        ->order_by('1')
                                        ->get()
                                        ->result();
        
        $dados['grupos_rede'] = $this ->db->select('COD_GRUPO_REDE, GRUPO_REDE')
                                        ->from('GRUPO_REDE')
                                        ->order_by('GRUPO_REDE', 'ASC')
                                        ->get()
                                        ->result();
        
        $dados['linhas_varejo'] = $this ->db->select('COD_LINHA_VAREJO, LINHA_VAREJO')
                                        ->from('LINHA_VAREJO')
                                        ->order_by('LINHA_VAREJO', 'ASC')
                                        ->get()
                                        ->result();
        
        $dados['estatisticas'] = $this ->db->select('COD_SITUACAO, SITUACAO')
                                        ->from('SITUACAO')
                                        ->order_by('SITUACAO', 'ASC')
                                        ->get()
                                        ->result();
        
        $dados['linhas_mercado'] = $this ->db->select('COD_LINHA_PRODUTOS, LINHA_PRODUTOS')
                                        ->from('LINHA_PRODUTOS')
                                        ->order_by('1')
                                        ->get()
                                        ->result();
        
        $dados['regioes_pais'] = $this ->db->select('REGIAO_PAIS')
                                        ->from('ESTADOS')
                                        ->order_by('1')
                                        ->distinct()
                                        ->get()
                                        ->result();
        
        $dados['segmentos'] = $this ->db->select('COD_SEGMENTO, SEGMENTO')
                                        ->from('SEGMENTO')
                                        ->order_by('SEGMENTO')
                                        ->get()
                                        ->result();
        
        $dados['status_geral'] = $this ->db->select('COD_STATUS, STATUS')
                                        ->from('STATUS')
                                        ->order_by('STATUS')
                                        ->get()
                                        ->result();
        
        $dados['marcas'] = $this ->db->select('ID, DESCRICAO')
                                        ->from('CLIENTES_MARCAS')
                                        ->order_by('DESCRICAO')
                                        ->get()
                                        ->result();
        
        $this->load->view('admin/template/index', $dados);
        
    }
    
    
    public function gera_relatorio_clientes($cliente_inicial = null, $cliente_final = null, $cnpj = null,
                                            $grupo_cadastro_inicial = null, $grupo_cadastro_final = null,
                                            $repres_inicial = null, $repres_final = null, $vend_inicial_input = null,
                                            $vend_final_input = null, $data_cadastro_inicial = null, $data_cadastro_final = null,
                                            $superv_inicial_input = null, $superv_final_input = null, $promotor_input = null,
                                            $fantasia_contem = null, $cidade_contem = null, $uf_contem = null, $carteira = null, 
                                            $shopping_contem = null, $linha_varejo = null, $estatistica= null, $linha_produtos = null,
                                            $data_recontato_inicial = null, $data_recontato_final = null, $data_aniversario_inicial= null, 
                                            $data_aniversario_final = null, $regiao = null, $segmento = null, $status = null, 
                                            $data_sintegra_inicial = null, $data_sintegra_final = null, $contato = null, $marca = null, $radio_button_clientes = null){
        
        $radio_button_clientes = $this->input->post('radio_button_clientes');
        
        $cliente_inicial = $this->input->post('cliente_inicial');
        $cliente_final = $this->input->post('cliente_final');
        $cnpj = $this->input->post('cnpj');
        $grupo_cadastro_inicial = $this->input->post('grupo_cadastro_inicial_input');
        $grupo_cadastro_final = $this->input->post('grupo_cadastro_final_input');
        $repres_inicial = $this->input->post('repres_inicial_input');
        $repres_final = $this->input->post('repres_final_input');
        $vend_inicial_input = $this->input->post('vend_inicial_input');
        $vend_final_input = $this->input->post('vend_final_input');
        $superv_inicial_input = $this->input->post('superv_inicial_input');
        $superv_final_input = $this->input->post('superv_final_input');
        $promotor_input = $this->input->post('promotor_input');
        $fantasia_contem = $this->input->post('fantasia_contem');
        $cidade_contem = $this->input->post('cidade_contem');
        $uf_contem = $this->input->post('uf');
        $rede_inicial = $this->input->post('rede_inicial_input');
        $rede_final = $this->input->post('rede_final_input');
        $carteira = $this->input->post('carteira_input');
        $shopping_contem = $this->input->post('shopping_contem');
        $data_cadastro_inicial = $this->input->post('data_cadastro_inicial');
        $data_cadastro_final =  $this->input->post('data_cadastro_final');
        $linha_varejo = $this->input->post('linha_varejo_input');
//        $data_compra_inicial = $this->input->post('data_compra_contato_inicial');
//        $data_compra_final = $this->input->post('data_compra_contato_final');
        $estatistica = $this->input->post('estatistica');
        $data_recontato_inicial = $this->input->post('data_reagenda_inicial');
        $data_recontato_final = $this->input->post('data_reagenda_final');
        $linha_produtos = $this->input->post('linha_mercado_');
        $data_aniversario_inicial = $this->input->post('data_aniversario_inicial');
        $data_aniversario_final = $this->input->post('data_aniversario_final');
        $regiao = $this->input->post('regiao_pais');
        $segmento = $this->input->post('segmento');
        $status = $this->input->post('status');
        $data_sintegra_inicial = $this->input->post('data_sintegra_incial');
        $data_sintegra_final = $this->input->post('data_sintegra_final');
        $contato = $this->input->post('nome_contato_contem');
        $marca = $this->input->post('marca');
        
        $whereCodGrupo = "GRUPOS_CLIFOR.COD_GRUPO > 0 AND GRUPOS_CLIFOR.COD_GRUPO <> 1 AND GRUPOS_CLIFOR.COD_GRUPO <> 6";
        
        if($cliente_inicial != null){
            $whereCliente = "AND CLIENTES.CODIGO >= ".$this->db->escape($cliente_inicial)." AND CLIENTES.CODIGO <= ". $this->db->escape($cliente_final);
        }else{
            $whereCliente = "";
        }
        if($cnpj != null){
            $whereCnpj = "AND CLIENTES.CGC =".$this->db->escape($cnpj);
        }else{
            $whereCnpj = "";
        }
        if($grupo_cadastro_inicial != null){
            $whereGrupoCadastro = "AND GRUPOS_CLIFOR.COD_GRUPO >= ".$this->db->escape($grupo_cadastro_inicial)." AND GRUPOS_CLIFOR.COD_GRUPO <= ".$this->db->escape($grupo_cadastro_final);
        }else{
            $whereGrupoCadastro = "";
        }
        if($repres_inicial != null){
            $whereRepres = "AND REPRESENTANTES.CODIGO >= ".$this->db->escape($repres_inicial)." AND REPRESENTANTES.CODIGO <= ".$this->db->escape($repres_final);
        }else{
            $whereRepres = "";
        }
        if($vend_inicial_input != null){
            $whereVend = "AND VENDEDORES.CODIGO >= ".$this->db->escape($vend_inicial_input)." AND VENDEDORES.CODIGO <= ".$this->db->escape($vend_final_input);
        }else{
            $whereVend = "";
        }
        if($superv_inicial_input != null){
            $whereSuperv = "AND SUPERVISORES.CODIGO >= ".$this->db->escape($superv_inicial_input)." AND SUPERVISORES.CODIGO <= ".$this->db->escape($superv_final_input);
        }else{
            $whereSuperv = "";
        }
        if($promotor_input != null){
            $wherePromo = "AND PROMOTORES.CODIGO = ".$this->db->escape($promotor_input);
        }else{
            $wherePromo = "";
        }
        if($fantasia_contem != null){
            $fantasia_upper = strtoupper($this->db->escape("%$fantasia_contem%"));
            $whereFantasia = "AND UPPER(CLIENTES.FANTASIA) LIKE ".$fantasia_upper;
        }else{
            $whereFantasia = "";
        }
        if($cidade_contem != null){
            $cidade_contem_upper = strtoupper($this->db->escape("%$cidade_contem%"));
            $whereCidade = "AND UPPER(CLIENTES.CIDADEFATURAMENTO) LIKE ".$cidade_contem_upper;
        }else{
            $whereCidade = "";
        }
        if($uf_contem != null){
            $uf_contem_upper = strtoupper($this->db->escape("%$uf_contem%"));
            $whereUf = "AND UPPER(CLIENTES.UFFATURAMENTO) LIKE ".$uf_contem_upper;
        }else{
            $whereUf = "";
        }
        if($rede_inicial != null){
            $whereRede = "AND GRUPO_REDE.COD_GRUPO_REDE >= ".$this->db->escape($rede_inicial)." AND GRUPO_REDE.COD_GRUPO_REDE <= ". $this->db->escape($rede_final);
        }else{
            $whereRede = "";
        }
        if($carteira != null){
            $whereCarteira = "AND CLIENTES.COD_CARTEIRA = ".$this->db->escape($carteira);
        }else{
            $whereCarteira = "";
        }
        if(($shopping_contem != null)&&($shopping_contem == "*")){
            $whereShopping = "AND CLIENTES.SHOPPING <> ''";
        }else{
            $whereShopping = "";
        }
        if($data_cadastro_inicial != null){
            $data_ini = date("d.m.Y", strtotime($data_cadastro_inicial));
            $data_fim = date("d.m.Y", strtotime($data_cadastro_final));
            $whereData = "AND CLIENTES.DATA_CADASTRO BETWEEN ".$this->db->escape($data_ini)." AND ".$this->db->escape($data_fim);
        }else{
            $whereData = "";
        }
        if($linha_varejo != null){
            $whereLinhaVarejo = "AND CLIENTES.COD_LINHA_VAREJO = ".$this->db->escape($linha_varejo);
        }else{
            $whereLinhaVarejo = "";
        }
//        if($data_compra_inicial != null){
//            $data_compra_ini = date("d.m.Y", strtotime($data_compra_inicial));
//            $data_compra_fim = date("d.m.Y", strtotime($data_compra_final));
//            $whereDataCompra = "AND CLIENTES.DATA_CONTATO BETWEEN ".$this->db->escape($data_compra_ini)." AND ".$this->db->escape($data_compra_fim);
//        }else{
//            $whereDataCompra = "";
//        }
        if($estatistica != null){
            $whereEstatistica = "AND CLIENTES.COD_SITUACAO = ".$this->db->escape($estatistica);
        }else{
            $whereEstatistica = "";
        }
        if($data_recontato_inicial != null){
            $data_recontato_ini = date("d.m.Y", strtotime($data_recontato_inicial));
            $data_recontato_fim = date("d.m.Y", strtotime($data_recontato_final));
            $whereDataRecontato = "AND CLIENTES.DATA_RECONTATO BETWEEN ".$this->db->escape($data_recontato_ini)." AND ".$this->db->escape($data_recontato_fim);
        }else{
            $whereDataRecontato = "";
        }
        if($linha_produtos != null){
            $whereLinhaProdutos = "AND CLIENTES.COD_LINHA_PRODUTOS = ".$this->db->escape($linha_produtos);
        }else{
            $whereLinhaProdutos = "";
        }
        if($data_aniversario_inicial != null){
            $data_aniver_ini = date("d.m.2000", strtotime($data_aniversario_inicial));
            $data_aniver_fim = date("d.m.2000", strtotime($data_aniversario_final));
            $whereDataAniver = "AND CLIENTES.ANIVERSARIO BETWEEN ".$this->db->escape($data_aniver_ini)." AND ".$this->db->escape($data_aniver_fim);
        }else{
            $whereDataAniver = "";
        }
        if($regiao != null){
            $whereRegiao = "AND ESTADOS.REGIAO_PAIS = ".$this->db->escape($regiao);
        }else{
            $whereRegiao = "";
        }
        if($segmento != null){
            $whereSegmento = "AND CLIENTES.COD_SEGMENTO = ".$this->db->escape($segmento);
        }else{
            $whereSegmento = "";
        }
        if($status != null){
            $whereStatus = "AND CLIENTES.COD_STATUS = ".$this->db->escape($status);
        }else{
            $whereStatus = "";
        }
        if($data_sintegra_inicial != null){
            $data_sintegra_ini = date("d.m.Y", strtotime($data_sintegra_inicial));
            $data_sintegra_fim = date("d.m.Y", strtotime($data_sintegra_final));
            $whereDataSintegra = "AND CLIENTES.DATA_SINTEGRA BETWEEN ".$this->db->escape($data_sintegra_ini)." AND ".$this->db->escape($data_sintegra_fim);
        }else{
            $whereDataSintegra = "";
        }
        if($contato != null){
            $contato_upper = strtoupper($this->db->escape("%$contato%"));
            $whereContato = "AND UPPER(CLIENTES.CONTATO) LIKE ".$contato_upper;
        }else{
            $whereContato = "";
        }
        if($marca != null){
            $whereMarca = "AND CLIENTES.COD_MARCA = ".$this->db->escape($marca);
        }else{
            $whereMarca = "";
        }
        
        switch($radio_button_clientes){
            
        
            case 'razao_radio':
                
            $dados['razoes'] = $this->db->select('CLIENTES.CODIGO, CLIENTES.NOME as NOME_CLIENTE,'
                                                . 'CLIENTES.COD_GRUPO, GRUPOS_CLIFOR.DESCRICAO, '
                                                . 'CLIENTES.COD_GRUPO_REDE, CLIENTES.CGC, CLIENTES.CIDADEFATURAMENTO, '
                                                . 'CLIENTES.UFFATURAMENTO, CLIENTES.BAIRROFATURAMENTO, '
                                                . 'REPRESENTANTES.NOME as NOME_REPRES, VENDEDORES.NOME as NOME_VEND, '
                                                . 'GRUPO_REDE.GRUPO_REDE, CLIENTES.DATA_RECONTATO')
                                        ->from('VENDEDORES')
                                        ->join('CLIENTES', 'VENDEDORES.CODIGO = CLIENTES.COD_VENDEDOR')
                                        ->join('PROMOTORES', 'CLIENTES.COD_PROMOTOR = PROMOTORES.CODIGO')
                                        ->join('SUPERVISORES', 'CLIENTES.COD_SUPERVISOR = SUPERVISORES.CODIGO')
                                        ->join('LINHA_PRODUTOS', 'CLIENTES.COD_LINHA_PRODUTOS = LINHA_PRODUTOS.COD_LINHA_PRODUTOS')
                                        ->join('ESTADOS', 'CLIENTES.UFFATURAMENTO = ESTADOS.UF')
                                        ->join('STATUS', 'CLIENTES.COD_STATUS = STATUS.COD_STATUS')
                                        ->join('SEGMENTO', 'CLIENTES.COD_SEGMENTO = SEGMENTO.COD_SEGMENTO')
                                        ->join('REPRESENTANTES', 'CLIENTES.REPRESENTANTE = REPRESENTANTES.CODIGO')
                                        ->join('GRUPOS_CLIFOR', 'CLIENTES.COD_GRUPO = GRUPOS_CLIFOR.COD_GRUPO')
                                        ->join('GRUPO_REDE', 'CLIENTES.COD_GRUPO_REDE = GRUPO_REDE.COD_GRUPO_REDE')
                                        ->where("$whereCodGrupo $whereGrupoCadastro $whereCliente "
                                              . "$whereCnpj $whereData $whereRepres $whereVend "
                                              . "$whereSuperv $wherePromo $whereFantasia $whereCidade "
                                              . "$whereUf $whereRede $whereCarteira $whereShopping "
                                              . "$whereLinhaVarejo $whereEstatistica $whereDataRecontato "
                                              . "$whereLinhaProdutos $whereDataAniver $whereRegiao $whereSegmento "
                                              . "$whereStatus $whereDataSintegra $whereContato $whereMarca")
                                        ->order_by('CLIENTES.NOME')
                                        ->get()
                                        ->result();
            
            $this->load->view('admin/relatorios/consulta_clientes_razao', $dados);
            break;
        
            case 'fantasia_radio':
                
            $dados['fantasias'] = $this->db->select('CLIENTES.CODIGO, CLIENTES.NOME as NOME_CLIENTE,'
                                                . 'CLIENTES.COD_GRUPO, GRUPOS_CLIFOR.DESCRICAO, '
                                                . 'CLIENTES.CGC, CLIENTES.CIDADEFATURAMENTO, '
                                                . 'CLIENTES.UFFATURAMENTO, CLIENTES.BAIRROFATURAMENTO, '
                                                . 'REPRESENTANTES.NOME as NOME_REPRES, VENDEDORES.NOME as NOME_VEND, '
                                                . 'CLIENTES.FANTASIA, GRUPO_REDE.GRUPO_REDE')
                                           ->from('CLIENTES')
                                           ->join('VENDEDORES', 'CLIENTES.COD_VENDEDOR = VENDEDORES.CODIGO')
                                           ->join('PROMOTORES', 'CLIENTES.COD_PROMOTOR = PROMOTORES.CODIGO')
                                           ->join('SUPERVISORES', 'CLIENTES.COD_SUPERVISOR = SUPERVISORES.CODIGO')
                                           ->join('LINHA_PRODUTOS', 'CLIENTES.COD_LINHA_PRODUTOS = LINHA_PRODUTOS.COD_LINHA_PRODUTOS')
                                           ->join('ESTADOS', 'CLIENTES.UFFATURAMENTO = ESTADOS.UF')
                                           ->join('STATUS', 'CLIENTES.COD_STATUS = STATUS.COD_STATUS')
                                           ->join('SEGMENTO', 'CLIENTES.COD_SEGMENTO = SEGMENTO.COD_SEGMENTO')
                                           ->join('REPRESENTANTES', 'REPRESENTANTES.CODIGO = CLIENTES.REPRESENTANTE')
                                           ->join('GRUPOS_CLIFOR', 'GRUPOS_CLIFOR.COD_GRUPO = CLIENTES.COD_GRUPO')
                                           ->join('GRUPO_REDE', 'CLIENTES.COD_GRUPO_REDE = GRUPO_REDE.COD_GRUPO_REDE')
                                           ->where("$whereCodGrupo $whereGrupoCadastro $whereCliente "
                                                  . "$whereCnpj $whereData $whereRepres $whereVend "
                                                  . "$whereSuperv $wherePromo $whereFantasia $whereCidade "
                                                  . "$whereUf $whereRede $whereCarteira $whereShopping "
                                                  . "$whereLinhaVarejo $whereEstatistica $whereDataRecontato "
                                                  . "$whereLinhaProdutos $whereDataAniver $whereRegiao $whereSegmento "
                                                  . "$whereStatus $whereDataSintegra $whereContato $whereMarca")
                                           ->order_by('CLIENTES.FANTASIA', 'ASC')
                                           ->get()
                                           ->result();
            
            $this->load->view('admin/relatorios/consulta_clientes_fantasia', $dados);
            break;
        
            case 'carteira_radio':
            
            $dados['carteiras'] = $this->db->select('CLIENTES.CODIGO, CLIENTES.NOME as NOME_CLIENTE,'
                                                . 'CLIENTES.COD_GRUPO, GRUPOS_CLIFOR.DESCRICAO, '
                                                . 'CLIENTES.CGC, CLIENTES.CIDADEFATURAMENTO, '
                                                . 'CLIENTES.UFFATURAMENTO, CLIENTES.BAIRROFATURAMENTO, '
                                                . 'REPRESENTANTES.NOME as NOME_REPRES, VENDEDORES.NOME as NOME_VEND, '
                                                . 'CLIENTES.FANTASIA, GRUPO_REDE.GRUPO_REDE, CLIENTES.COD_CARTEIRA, '
                                                . 'CARTEIRA.CARTEIRA, CLIENTES.QTD_VENDEDORES')
                                           ->from('CLIENTES')
                                           ->join('VENDEDORES', 'CLIENTES.COD_VENDEDOR = VENDEDORES.CODIGO')
                                           ->join('PROMOTORES', 'CLIENTES.COD_PROMOTOR = PROMOTORES.CODIGO')
                                           ->join('CARTEIRA', 'CLIENTES.COD_CARTEIRA = CARTEIRA.COD_CARTEIRA')
                                           ->join('SUPERVISORES', 'CLIENTES.COD_SUPERVISOR = SUPERVISORES.CODIGO')
                                           ->join('LINHA_PRODUTOS', 'CLIENTES.COD_LINHA_PRODUTOS = LINHA_PRODUTOS.COD_LINHA_PRODUTOS')
                                           ->join('ESTADOS', 'CLIENTES.UFFATURAMENTO = ESTADOS.UF')
                                           ->join('STATUS', 'CLIENTES.COD_STATUS = STATUS.COD_STATUS')
                                           ->join('SEGMENTO', 'CLIENTES.COD_SEGMENTO = SEGMENTO.COD_SEGMENTO')
                                           ->join('REPRESENTANTES', 'REPRESENTANTES.CODIGO = CLIENTES.REPRESENTANTE')
                                           ->join('GRUPOS_CLIFOR', 'GRUPOS_CLIFOR.COD_GRUPO = CLIENTES.COD_GRUPO')
                                           ->join('GRUPO_REDE', 'CLIENTES.COD_GRUPO_REDE = GRUPO_REDE.COD_GRUPO_REDE')
                                           ->where("$whereCodGrupo $whereGrupoCadastro $whereCliente "
                                                  . "$whereCnpj $whereData $whereRepres $whereVend "
                                                  . "$whereSuperv $wherePromo $whereFantasia $whereCidade "
                                                  . "$whereUf $whereRede $whereCarteira $whereShopping "
                                                  . "$whereLinhaVarejo $whereEstatistica $whereDataRecontato "
                                                  . "$whereLinhaProdutos $whereDataAniver $whereRegiao $whereSegmento "
                                                  . "$whereStatus $whereDataSintegra $whereContato $whereMarca")
                                           ->order_by('CLIENTES.COD_CARTEIRA', 'ASC')
                                           ->order_by('CLIENTES.NOME', 'ASC')
                                           ->get()
                                           ->result();
            
            $this->load->view('admin/relatorios/consulta_clientes_carteira', $dados);
            break;
        
            case 'cidade_radio':
            
            $dados['cidades'] = $this->db->select('CLIENTES.CODIGO, CLIENTES.NOME as NOME_CLIENTE,'
                                                . 'CLIENTES.COD_GRUPO, GRUPOS_CLIFOR.DESCRICAO, '
                                                . 'CLIENTES.CGC, CLIENTES.CIDADEFATURAMENTO, CLIENTES.ENDERECOFATURAMENTO,'
                                                . 'CLIENTES.UFFATURAMENTO, CLIENTES.BAIRROFATURAMENTO, '
                                                . 'REPRESENTANTES.NOME as NOME_REPRES, VENDEDORES.NOME as NOME_VEND, '
                                                . 'CLIENTES.FANTASIA, GRUPO_REDE.GRUPO_REDE, CLIENTES.COD_CARTEIRA, '
                                                . 'CLIENTES.FONE1, CARTEIRA.CARTEIRA, CLIENTES.QTD_VENDEDORES')
                                           ->from('CLIENTES')
                                           ->join('VENDEDORES', 'CLIENTES.COD_VENDEDOR = VENDEDORES.CODIGO')
                                           ->join('PROMOTORES', 'CLIENTES.COD_PROMOTOR = PROMOTORES.CODIGO')
                                           ->join('CARTEIRA', 'CLIENTES.COD_CARTEIRA = CARTEIRA.COD_CARTEIRA')
                                           ->join('SUPERVISORES', 'CLIENTES.COD_SUPERVISOR = SUPERVISORES.CODIGO')
                                           ->join('LINHA_PRODUTOS', 'CLIENTES.COD_LINHA_PRODUTOS = LINHA_PRODUTOS.COD_LINHA_PRODUTOS')
                                           ->join('ESTADOS', 'CLIENTES.UFFATURAMENTO = ESTADOS.UF')
                                           ->join('STATUS', 'CLIENTES.COD_STATUS = STATUS.COD_STATUS')
                                           ->join('SEGMENTO', 'CLIENTES.COD_SEGMENTO = SEGMENTO.COD_SEGMENTO')
                                           ->join('REPRESENTANTES', 'REPRESENTANTES.CODIGO = CLIENTES.REPRESENTANTE')
                                           ->join('GRUPOS_CLIFOR', 'GRUPOS_CLIFOR.COD_GRUPO = CLIENTES.COD_GRUPO')
                                           ->join('GRUPO_REDE', 'CLIENTES.COD_GRUPO_REDE = GRUPO_REDE.COD_GRUPO_REDE')
                                           ->where("$whereCodGrupo $whereGrupoCadastro $whereCliente "
                                                  . "$whereCnpj $whereData $whereRepres $whereVend "
                                                  . "$whereSuperv $wherePromo $whereFantasia $whereCidade "
                                                  . "$whereUf $whereRede $whereCarteira $whereShopping "
                                                  . "$whereLinhaVarejo $whereEstatistica $whereDataRecontato "
                                                  . "$whereLinhaProdutos $whereDataAniver $whereRegiao $whereSegmento "
                                                  . "$whereStatus $whereDataSintegra $whereContato $whereMarca")
                                           ->order_by('CLIENTES.CIDADEFATURAMENTO', 'ASC')
                                           ->order_by('CARTEIRA.TIPO', 'ASC')
                                           ->order_by('CLIENTES.FANTASIA', 'ASC')
                                           ->get()
                                           ->result();
            
            $this->load->view('admin/relatorios/consulta_clientes_cidade', $dados);
            break;
        
            case 'cidade_carteira':
            
            $dados['cidades_carteiras'] = $this->db->select('CLIENTES.CODIGO, CLIENTES.NOME as NOME_CLIENTE,'
                                                . 'CLIENTES.COD_GRUPO, GRUPOS_CLIFOR.DESCRICAO, '
                                                . 'CLIENTES.CGC, CLIENTES.CIDADEFATURAMENTO, CLIENTES.ENDERECOFATURAMENTO,'
                                                . 'CLIENTES.UFFATURAMENTO, CLIENTES.BAIRROFATURAMENTO, '
                                                . 'REPRESENTANTES.NOME as NOME_REPRES, VENDEDORES.NOME as NOME_VEND, '
                                                . 'CLIENTES.FANTASIA, GRUPO_REDE.GRUPO_REDE, CLIENTES.COD_CARTEIRA, '
                                                . 'CLIENTES.FONE1, CARTEIRA.CARTEIRA,CARTEIRA.TIPO, CLIENTES.QTD_VENDEDORES, CLIENTES.CEPFATURAMENTO')
                                           ->from('CLIENTES')
                                           ->join('VENDEDORES', 'CLIENTES.COD_VENDEDOR = VENDEDORES.CODIGO')
                                           ->join('PROMOTORES', 'CLIENTES.COD_PROMOTOR = PROMOTORES.CODIGO')
                                           ->join('CARTEIRA', 'CLIENTES.COD_CARTEIRA = CARTEIRA.COD_CARTEIRA')
                                           ->join('SUPERVISORES', 'CLIENTES.COD_SUPERVISOR = SUPERVISORES.CODIGO')
                                           ->join('LINHA_PRODUTOS', 'CLIENTES.COD_LINHA_PRODUTOS = LINHA_PRODUTOS.COD_LINHA_PRODUTOS')
                                           ->join('ESTADOS', 'CLIENTES.UFFATURAMENTO = ESTADOS.UF')
                                           ->join('STATUS', 'CLIENTES.COD_STATUS = STATUS.COD_STATUS')
                                           ->join('SEGMENTO', 'CLIENTES.COD_SEGMENTO = SEGMENTO.COD_SEGMENTO')
                                           ->join('REPRESENTANTES', 'REPRESENTANTES.CODIGO = CLIENTES.REPRESENTANTE')
                                           ->join('GRUPOS_CLIFOR', 'GRUPOS_CLIFOR.COD_GRUPO = CLIENTES.COD_GRUPO')
                                           ->join('GRUPO_REDE', 'CLIENTES.COD_GRUPO_REDE = GRUPO_REDE.COD_GRUPO_REDE')
                                           ->where("$whereCodGrupo $whereGrupoCadastro $whereCliente "
                                                  . "$whereCnpj $whereData $whereRepres $whereVend "
                                                  . "$whereSuperv $wherePromo $whereFantasia $whereCidade "
                                                  . "$whereUf $whereRede $whereCarteira $whereShopping "
                                                  . "$whereLinhaVarejo $whereEstatistica $whereDataRecontato "
                                                  . "$whereLinhaProdutos $whereDataAniver $whereRegiao $whereSegmento "
                                                  . "$whereStatus $whereDataSintegra $whereContato $whereMarca")
                                           ->order_by('CLIENTES.CIDADEFATURAMENTO', 'ASC')
                                           ->order_by('CARTEIRA.TIPO', 'ASC')
                                           ->order_by('CLIENTES.FANTASIA', 'ASC')
                                           ->get()
                                           ->result();
            
            $this->load->view('admin/relatorios/consulta_clientes_cidade_carteira', $dados);
            break;
        
            case 'uf_radio':
                
            $dados['ufs'] = $this->db->select('CLIENTES.CODIGO, CLIENTES.NOME as NOME_CLIENTE,'
                                                . 'CLIENTES.COD_GRUPO, GRUPOS_CLIFOR.DESCRICAO, '
                                                . 'CLIENTES.CGC, CLIENTES.CIDADEFATURAMENTO, CLIENTES.ENDERECOFATURAMENTO,'
                                                . 'CLIENTES.UFFATURAMENTO, CLIENTES.BAIRROFATURAMENTO, '
                                                . 'REPRESENTANTES.NOME as NOME_REPRES, VENDEDORES.NOME as NOME_VEND, '
                                                . 'CLIENTES.FANTASIA, GRUPO_REDE.GRUPO_REDE, CLIENTES.COD_CARTEIRA, '
                                                . 'CLIENTES.FONE1, CARTEIRA.CARTEIRA,CARTEIRA.TIPO, CLIENTES.QTD_VENDEDORES, CLIENTES.CEPFATURAMENTO')
                                           ->from('CLIENTES')
                                           ->join('VENDEDORES', 'CLIENTES.COD_VENDEDOR = VENDEDORES.CODIGO')
                                           ->join('PROMOTORES', 'CLIENTES.COD_PROMOTOR = PROMOTORES.CODIGO')
                                           ->join('CARTEIRA', 'CLIENTES.COD_CARTEIRA = CARTEIRA.COD_CARTEIRA')
                                           ->join('SUPERVISORES', 'CLIENTES.COD_SUPERVISOR = SUPERVISORES.CODIGO')
                                           ->join('LINHA_PRODUTOS', 'CLIENTES.COD_LINHA_PRODUTOS = LINHA_PRODUTOS.COD_LINHA_PRODUTOS')
                                           ->join('ESTADOS', 'CLIENTES.UFFATURAMENTO = ESTADOS.UF')
                                           ->join('STATUS', 'CLIENTES.COD_STATUS = STATUS.COD_STATUS')
                                           ->join('SEGMENTO', 'CLIENTES.COD_SEGMENTO = SEGMENTO.COD_SEGMENTO')
                                           ->join('REPRESENTANTES', 'REPRESENTANTES.CODIGO = CLIENTES.REPRESENTANTE')
                                           ->join('GRUPOS_CLIFOR', 'GRUPOS_CLIFOR.COD_GRUPO = CLIENTES.COD_GRUPO')
                                           ->join('GRUPO_REDE', 'CLIENTES.COD_GRUPO_REDE = GRUPO_REDE.COD_GRUPO_REDE')
                                           ->where("$whereCodGrupo $whereGrupoCadastro $whereCliente "
                                                  . "$whereCnpj $whereData $whereRepres $whereVend "
                                                  . "$whereSuperv $wherePromo $whereFantasia $whereCidade "
                                                  . "$whereUf $whereRede $whereCarteira $whereShopping "
                                                  . "$whereLinhaVarejo $whereEstatistica $whereDataRecontato "
                                                  . "$whereLinhaProdutos $whereDataAniver $whereRegiao $whereSegmento "
                                                  . "$whereStatus $whereDataSintegra $whereContato $whereMarca")
                                           ->order_by('CLIENTES.UFFATURAMENTO', 'ASC')
                                           ->order_by('CLIENTES.CIDADEFATURAMENTO', 'ASC')
                                           ->order_by('CLIENTES.NOME', 'ASC')
                                           ->get()
                                           ->result();
            
            $this->load->view('admin/relatorios/consulta_clientes_uf', $dados);
            break;
        
            case 'rede_radio';
            
            $dados['redes'] = $this->db->select('CLIENTES.CODIGO, CLIENTES.NOME as NOME_CLIENTE,'
                                                . 'CLIENTES.COD_GRUPO, GRUPOS_CLIFOR.DESCRICAO, '
                                                . 'CLIENTES.CGC, CLIENTES.CIDADEFATURAMENTO, CLIENTES.ENDERECOFATURAMENTO,'
                                                . 'CLIENTES.UFFATURAMENTO, CLIENTES.BAIRROFATURAMENTO, '
                                                . 'REPRESENTANTES.NOME as NOME_REPRES, VENDEDORES.NOME as NOME_VEND, '
                                                . 'CLIENTES.FANTASIA, GRUPO_REDE.GRUPO_REDE, CLIENTES.COD_CARTEIRA, '
                                                . 'CLIENTES.FONE1,CLIENTES.COD_GRUPO_REDE, CARTEIRA.CARTEIRA,CARTEIRA.TIPO, '
                                                . 'CLIENTES.QTD_VENDEDORES, CLIENTES.CEPFATURAMENTO')
                                           ->from('CLIENTES')
                                           ->join('VENDEDORES', 'CLIENTES.COD_VENDEDOR = VENDEDORES.CODIGO')
                                           ->join('PROMOTORES', 'CLIENTES.COD_PROMOTOR = PROMOTORES.CODIGO')
                                           ->join('CARTEIRA', 'CLIENTES.COD_CARTEIRA = CARTEIRA.COD_CARTEIRA')
                                           ->join('SUPERVISORES', 'CLIENTES.COD_SUPERVISOR = SUPERVISORES.CODIGO')
                                           ->join('LINHA_PRODUTOS', 'CLIENTES.COD_LINHA_PRODUTOS = LINHA_PRODUTOS.COD_LINHA_PRODUTOS')
                                           ->join('ESTADOS', 'CLIENTES.UFFATURAMENTO = ESTADOS.UF')
                                           ->join('STATUS', 'CLIENTES.COD_STATUS = STATUS.COD_STATUS')
                                           ->join('SEGMENTO', 'CLIENTES.COD_SEGMENTO = SEGMENTO.COD_SEGMENTO')
                                           ->join('REPRESENTANTES', 'REPRESENTANTES.CODIGO = CLIENTES.REPRESENTANTE')
                                           ->join('GRUPOS_CLIFOR', 'GRUPOS_CLIFOR.COD_GRUPO = CLIENTES.COD_GRUPO')
                                           ->join('GRUPO_REDE', 'CLIENTES.COD_GRUPO_REDE = GRUPO_REDE.COD_GRUPO_REDE')
                                           ->where("$whereCodGrupo $whereGrupoCadastro $whereCliente "
                                                  . "$whereCnpj $whereData $whereRepres $whereVend "
                                                  . "$whereSuperv $wherePromo $whereFantasia $whereCidade "
                                                  . "$whereUf $whereRede $whereCarteira $whereShopping "
                                                  . "$whereLinhaVarejo $whereEstatistica $whereDataRecontato "
                                                  . "$whereLinhaProdutos $whereDataAniver $whereRegiao $whereSegmento "
                                                  . "$whereStatus $whereDataSintegra $whereContato $whereMarca")
                                           ->order_by('CLIENTES.COD_GRUPO_REDE', 'ASC')
                                           ->order_by('CLIENTES.NOME', 'ASC')
                                           ->get()
                                           ->result();
            
            $this->load->view('admin/relatorios/consulta_clientes_rede', $dados);
            break;
        
            case 'rede_sintetico_radio':
        
            $dados['redes_sinteticos'] = $this->db->select('GRUPO_REDE.GRUPO_REDE, CLIENTES.COD_GRUPO_REDE, COUNT(CLIENTES.CODIGO) as SOMA')
                                           ->from('CLIENTES')
                                           ->join('VENDEDORES', 'CLIENTES.COD_VENDEDOR = VENDEDORES.CODIGO')
                                           ->join('PROMOTORES', 'CLIENTES.COD_PROMOTOR = PROMOTORES.CODIGO')
                                           ->join('CARTEIRA', 'CLIENTES.COD_CARTEIRA = CARTEIRA.COD_CARTEIRA')
                                           ->join('SUPERVISORES', 'CLIENTES.COD_SUPERVISOR = SUPERVISORES.CODIGO')
                                           ->join('LINHA_PRODUTOS', 'CLIENTES.COD_LINHA_PRODUTOS = LINHA_PRODUTOS.COD_LINHA_PRODUTOS')
                                           ->join('ESTADOS', 'CLIENTES.UFFATURAMENTO = ESTADOS.UF')
                                           ->join('STATUS', 'CLIENTES.COD_STATUS = STATUS.COD_STATUS')
                                           ->join('SEGMENTO', 'CLIENTES.COD_SEGMENTO = SEGMENTO.COD_SEGMENTO')
                                           ->join('REPRESENTANTES', 'REPRESENTANTES.CODIGO = CLIENTES.REPRESENTANTE')
                                           ->join('GRUPOS_CLIFOR', 'GRUPOS_CLIFOR.COD_GRUPO = CLIENTES.COD_GRUPO')
                                           ->join('GRUPO_REDE', 'CLIENTES.COD_GRUPO_REDE = GRUPO_REDE.COD_GRUPO_REDE')
                                           ->where("$whereCodGrupo $whereGrupoCadastro $whereCliente "
                                                  . "$whereCnpj $whereData $whereRepres $whereVend "
                                                  . "$whereSuperv $wherePromo $whereFantasia $whereCidade "
                                                  . "$whereUf $whereRede $whereCarteira $whereShopping "
                                                  . "$whereLinhaVarejo $whereEstatistica $whereDataRecontato "
                                                  . "$whereLinhaProdutos $whereDataAniver $whereRegiao $whereSegmento "
                                                  . "$whereStatus $whereDataSintegra $whereContato $whereMarca")
                                           ->group_by('CLIENTES.COD_GRUPO_REDE')
                                           ->group_by('GRUPO_REDE.GRUPO_REDE')
                                           ->get()
                                           ->result();
            
            $this->load->view('admin/relatorios/consulta_clientes_rede_sintetico', $dados);
            break;
            
            case 'linha_mercado_radio':
            
            $dados['linhas_produtos'] = $this->db->select('CLIENTES.CODIGO, CLIENTES.NOME as NOME_CLIENTE,'
                                                . 'CLIENTES.COD_GRUPO, GRUPOS_CLIFOR.DESCRICAO, CLIENTES.COD_LINHA_PRODUTOS, '
                                                . 'CLIENTES.CGC, CLIENTES.CIDADEFATURAMENTO, CLIENTES.ENDERECOFATURAMENTO,'
                                                . 'CLIENTES.UFFATURAMENTO, CLIENTES.BAIRROFATURAMENTO, LINHA_PRODUTOS.LINHA_PRODUTOS, '
                                                . 'REPRESENTANTES.NOME as NOME_REPRES, VENDEDORES.NOME as NOME_VEND, '
                                                . 'CLIENTES.FANTASIA, GRUPO_REDE.GRUPO_REDE, CLIENTES.COD_CARTEIRA, '
                                                . 'CLIENTES.FONE1, CARTEIRA.CARTEIRA,CARTEIRA.TIPO, CLIENTES.QTD_VENDEDORES, CLIENTES.CEPFATURAMENTO')
                                           ->from('CLIENTES')
                                           ->join('VENDEDORES', 'CLIENTES.COD_VENDEDOR = VENDEDORES.CODIGO')
                                           ->join('PROMOTORES', 'CLIENTES.COD_PROMOTOR = PROMOTORES.CODIGO')
                                           ->join('CARTEIRA', 'CLIENTES.COD_CARTEIRA = CARTEIRA.COD_CARTEIRA')
                                           ->join('SUPERVISORES', 'CLIENTES.COD_SUPERVISOR = SUPERVISORES.CODIGO')
                                           ->join('LINHA_PRODUTOS', 'CLIENTES.COD_LINHA_PRODUTOS = LINHA_PRODUTOS.COD_LINHA_PRODUTOS')
                                           ->join('ESTADOS', 'CLIENTES.UFFATURAMENTO = ESTADOS.UF')
                                           ->join('STATUS', 'CLIENTES.COD_STATUS = STATUS.COD_STATUS')
                                           ->join('SEGMENTO', 'CLIENTES.COD_SEGMENTO = SEGMENTO.COD_SEGMENTO')
                                           ->join('REPRESENTANTES', 'REPRESENTANTES.CODIGO = CLIENTES.REPRESENTANTE')
                                           ->join('GRUPOS_CLIFOR', 'GRUPOS_CLIFOR.COD_GRUPO = CLIENTES.COD_GRUPO')
                                           ->join('GRUPO_REDE', 'CLIENTES.COD_GRUPO_REDE = GRUPO_REDE.COD_GRUPO_REDE')
                                           ->where("$whereCodGrupo $whereGrupoCadastro $whereCliente "
                                                  . "$whereCnpj $whereData $whereRepres $whereVend "
                                                  . "$whereSuperv $wherePromo $whereFantasia $whereCidade "
                                                  . "$whereUf $whereRede $whereCarteira $whereShopping "
                                                  . "$whereLinhaVarejo $whereEstatistica $whereDataRecontato "
                                                  . "$whereLinhaProdutos $whereDataAniver $whereRegiao $whereSegmento "
                                                  . "$whereStatus $whereDataSintegra $whereContato $whereMarca")
                                           ->order_by('LINHA_PRODUTOS.LINHA_PRODUTOS', 'ASC')
                                           ->get()
                                           ->result();
            
            $this->load->view('admin/relatorios/consulta_clientes_linha_produto', $dados);
            break;
            
            case 'linha_varejo_radio':
            
            $dados['linhas_varejos'] = $this->db->select('CLIENTES.CODIGO, CLIENTES.NOME as NOME_CLIENTE,'
                                                . 'CLIENTES.COD_GRUPO, GRUPOS_CLIFOR.DESCRICAO, CLIENTES.COD_LINHA_VAREJO, '
                                                . 'CLIENTES.CGC, CLIENTES.CIDADEFATURAMENTO, CLIENTES.ENDERECOFATURAMENTO,'
                                                . 'CLIENTES.UFFATURAMENTO, CLIENTES.BAIRROFATURAMENTO, LINHA_VAREJO.LINHA_VAREJO, '
                                                . 'REPRESENTANTES.NOME as NOME_REPRES, VENDEDORES.NOME as NOME_VEND, '
                                                . 'CLIENTES.FANTASIA, GRUPO_REDE.GRUPO_REDE, CLIENTES.COD_CARTEIRA, '
                                                . 'CLIENTES.FONE1, CARTEIRA.CARTEIRA,CARTEIRA.TIPO, CLIENTES.QTD_VENDEDORES, CLIENTES.CEPFATURAMENTO')
                                           ->from('CLIENTES')
                                           ->join('VENDEDORES', 'CLIENTES.COD_VENDEDOR = VENDEDORES.CODIGO')
                                           ->join('PROMOTORES', 'CLIENTES.COD_PROMOTOR = PROMOTORES.CODIGO')
                                           ->join('CARTEIRA', 'CLIENTES.COD_CARTEIRA = CARTEIRA.COD_CARTEIRA')
                                           ->join('SUPERVISORES', 'CLIENTES.COD_SUPERVISOR = SUPERVISORES.CODIGO')
                                           ->join('LINHA_PRODUTOS', 'CLIENTES.COD_LINHA_PRODUTOS = LINHA_PRODUTOS.COD_LINHA_PRODUTOS')
                                           ->join('LINHA_VAREJO', 'CLIENTES.COD_LINHA_VAREJO = LINHA_VAREJO.COD_LINHA_VAREJO')
                                           ->join('ESTADOS', 'CLIENTES.UFFATURAMENTO = ESTADOS.UF')
                                           ->join('STATUS', 'CLIENTES.COD_STATUS = STATUS.COD_STATUS')
                                           ->join('SEGMENTO', 'CLIENTES.COD_SEGMENTO = SEGMENTO.COD_SEGMENTO')
                                           ->join('REPRESENTANTES', 'REPRESENTANTES.CODIGO = CLIENTES.REPRESENTANTE')
                                           ->join('GRUPOS_CLIFOR', 'GRUPOS_CLIFOR.COD_GRUPO = CLIENTES.COD_GRUPO')
                                           ->join('GRUPO_REDE', 'CLIENTES.COD_GRUPO_REDE = GRUPO_REDE.COD_GRUPO_REDE')
                                           ->where("$whereCodGrupo $whereGrupoCadastro $whereCliente "
                                                  . "$whereCnpj $whereData $whereRepres $whereVend "
                                                  . "$whereSuperv $wherePromo $whereFantasia $whereCidade "
                                                  . "$whereUf $whereRede $whereCarteira $whereShopping "
                                                  . "$whereLinhaVarejo $whereEstatistica $whereDataRecontato "
                                                  . "$whereLinhaProdutos $whereDataAniver $whereRegiao $whereSegmento "
                                                  . "$whereStatus $whereDataSintegra $whereContato $whereMarca")
                                           ->order_by('LINHA_VAREJO.LINHA_VAREJO', 'ASC')
                                           ->get()
                                           ->result();
            
            $this->load->view('admin/relatorios/consulta_clientes_linha_varejo', $dados);
            break;
        
            case 'recontato_radio':
                
            $dados['recontatos'] = $this->db->select('CLIENTES.CODIGO, CLIENTES.NOME as NOME_CLIENTE,'
                                                . 'CLIENTES.COD_GRUPO, GRUPOS_CLIFOR.DESCRICAO,'
                                                . 'CLIENTES.CGC, CLIENTES.CIDADEFATURAMENTO, CLIENTES.ENDERECOFATURAMENTO,'
                                                . 'CLIENTES.UFFATURAMENTO, CLIENTES.BAIRROFATURAMENTO, CLIENTES.DATA_RECONTATO, '
                                                . 'REPRESENTANTES.NOME as NOME_REPRES, VENDEDORES.NOME as NOME_VEND, '
                                                . 'GRUPO_REDE.GRUPO_REDE, CLIENTES.COD_CARTEIRA, CLIENTES.COD_GRUPO_REDE, '
                                                . 'CLIENTES.CEPFATURAMENTO')
                                           ->from('CLIENTES')
                                           ->join('VENDEDORES', 'CLIENTES.COD_VENDEDOR = VENDEDORES.CODIGO')
                                           ->join('PROMOTORES', 'CLIENTES.COD_PROMOTOR = PROMOTORES.CODIGO')
                                           ->join('CARTEIRA', 'CLIENTES.COD_CARTEIRA = CARTEIRA.COD_CARTEIRA')
                                           ->join('SUPERVISORES', 'CLIENTES.COD_SUPERVISOR = SUPERVISORES.CODIGO')
                                           ->join('LINHA_PRODUTOS', 'CLIENTES.COD_LINHA_PRODUTOS = LINHA_PRODUTOS.COD_LINHA_PRODUTOS')
                                           ->join('LINHA_VAREJO', 'CLIENTES.COD_LINHA_VAREJO = LINHA_VAREJO.COD_LINHA_VAREJO')
                                           ->join('ESTADOS', 'CLIENTES.UFFATURAMENTO = ESTADOS.UF')
                                           ->join('STATUS', 'CLIENTES.COD_STATUS = STATUS.COD_STATUS')
                                           ->join('SEGMENTO', 'CLIENTES.COD_SEGMENTO = SEGMENTO.COD_SEGMENTO')
                                           ->join('REPRESENTANTES', 'REPRESENTANTES.CODIGO = CLIENTES.REPRESENTANTE')
                                           ->join('GRUPOS_CLIFOR', 'GRUPOS_CLIFOR.COD_GRUPO = CLIENTES.COD_GRUPO')
                                           ->join('GRUPO_REDE', 'CLIENTES.COD_GRUPO_REDE = GRUPO_REDE.COD_GRUPO_REDE')
                                           ->where("$whereCodGrupo $whereGrupoCadastro $whereCliente "
                                                  . "$whereCnpj $whereData $whereRepres $whereVend "
                                                  . "$whereSuperv $wherePromo $whereFantasia $whereCidade "
                                                  . "$whereUf $whereRede $whereCarteira $whereShopping "
                                                  . "$whereLinhaVarejo $whereEstatistica $whereDataRecontato "
                                                  . "$whereLinhaProdutos $whereDataAniver $whereRegiao $whereSegmento "
                                                  . "$whereStatus $whereDataSintegra $whereContato $whereMarca")
                                           ->order_by('CLIENTES.DATA_RECONTATO')
                                           ->get()
                                           ->result();
            
            $this->load->view('admin/relatorios/consulta_clientes_recontato', $dados);
            break;
        
            case 'reagenda_dia_radio':
            
            $dados['reagendado_dia'] = $this->db->select('COUNT(CLIENTES.CODIGO) AS SOMA, '
                                                        . 'CLIENTES.DATA_RECONTATO, CLIENTES.COD_CARTEIRA')
                                           ->from('CLIENTES')
                                           ->join('VENDEDORES', 'CLIENTES.COD_VENDEDOR = VENDEDORES.CODIGO')
                                           ->join('PROMOTORES', 'CLIENTES.COD_PROMOTOR = PROMOTORES.CODIGO')
                                           ->join('CARTEIRA', 'CLIENTES.COD_CARTEIRA = CARTEIRA.COD_CARTEIRA')
                                           ->join('SUPERVISORES', 'CLIENTES.COD_SUPERVISOR = SUPERVISORES.CODIGO')
                                           ->join('LINHA_PRODUTOS', 'CLIENTES.COD_LINHA_PRODUTOS = LINHA_PRODUTOS.COD_LINHA_PRODUTOS')
                                           ->join('ESTADOS', 'CLIENTES.UFFATURAMENTO = ESTADOS.UF')
                                           ->join('STATUS', 'CLIENTES.COD_STATUS = STATUS.COD_STATUS')
                                           ->join('SEGMENTO', 'CLIENTES.COD_SEGMENTO = SEGMENTO.COD_SEGMENTO')
                                           ->join('REPRESENTANTES', 'REPRESENTANTES.CODIGO = CLIENTES.REPRESENTANTE')
                                           ->join('GRUPOS_CLIFOR', 'GRUPOS_CLIFOR.COD_GRUPO = CLIENTES.COD_GRUPO')
                                           ->join('GRUPO_REDE', 'CLIENTES.COD_GRUPO_REDE = GRUPO_REDE.COD_GRUPO_REDE')
                                           ->where("$whereCodGrupo $whereGrupoCadastro $whereCliente "
                                                  . "$whereCnpj $whereData $whereRepres $whereVend "
                                                  . "$whereSuperv $wherePromo $whereFantasia $whereCidade "
                                                  . "$whereUf $whereRede $whereCarteira $whereShopping "
                                                  . "$whereLinhaVarejo $whereEstatistica $whereDataRecontato "
                                                  . "$whereLinhaProdutos $whereDataAniver $whereRegiao $whereSegmento "
                                                  . "$whereStatus $whereDataSintegra $whereContato $whereMarca")
                                           ->group_by('CLIENTES.COD_CARTEIRA')
                                           ->group_by('CLIENTES.DATA_RECONTATO')
                                           ->order_by('CLIENTES.COD_CARTEIRA')
                                           ->order_by('CLIENTES.DATA_RECONTATO')
                                           ->get()
                                           ->result();
            
            $this->load->view('admin/relatorios/consulta_clientes_reagendados_dia', $dados);
            break;
        }
        
        
        
    }
    
}