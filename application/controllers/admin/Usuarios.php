<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {

    function __construct(){
        
        parent::__construct();
        $logado = $this->session->userdata("logado");
	if ($logado != 1) redirect(base_url('admin/login'));	
        
    }

    function index() {
        $dados['titulo'] = "Usuários Ativos";
        $dados['subtitulo'] = "Filtrar";
        $dados['url'] = 'admin/usuarios';
        
        $this->load->view('admin/template/index', $dados);
        
    }
    
    
    public function usuariosAtivos() {
      $draw = intval($this->input->get("draw"));
      $start = intval($this->input->get("start"));
      $length = intval($this->input->get("length"));

      $query = $this ->db->select('NOME, COD_USUARIO, RAMAL')
                         ->from('USUARIOS')
                         ->where('COD_USUARIO != ', 0)
                         ->where('ATIVO', 'S')
                         ->order_by('NOME', 'ASC')
                         ->order_by('ATIVO', 'ASC')
                         ->get();
      $data = [];
        
      foreach($query->result() as $r) {
      $alterar= anchor(base_url('admin/usuarios/alterar/'.$r->COD_USUARIO),'<i class="fa fa-refresh fa-fw"></i> Alterar');
           $data[] = array(
                $r->NOME,
                $r->RAMAL,
                $alterar
           );
      }

      $result = array(
               "draw" => $draw,
               "recordsTotal" => $query->num_rows(),
               "recordsFiltered" => $query->num_rows(),
               "data" => $data
            );

      echo json_encode($result);
      
      exit();
   }
   
   public function usuariosInativos() {
      $draw = intval($this->input->get("draw"));
      $start = intval($this->input->get("start"));
      $length = intval($this->input->get("length"));

      $query = $this ->db->select('NOME, COD_USUARIO, RAMAL')
                         ->from('USUARIOS')
                         ->where('COD_USUARIO != ', 0)
                         ->where('ATIVO', 'N')
                         ->order_by('NOME', 'ASC')
                         ->order_by('ATIVO', 'ASC')
                         ->get();
        $data = [];
      
      foreach($query->result() as $r) {
      $alterar= anchor(base_url('admin/usuarios/alterar/'.$r->COD_USUARIO),'<i class="fa fa-refresh fa-fw"></i> Alterar');
           $data[] = array(
                $r->NOME,
                $r->RAMAL,
                $alterar
           );
      }

      $result = array(
               "draw" => $draw,
               "recordsTotal" => $query->num_rows(),
               "recordsFiltered" => $query->num_rows(),
               "data" => $data
            );

      echo json_encode($result);
      
      exit();
   }
   
   
   
   
    function alterar($id) {
        
        $dados['titulo'] = "Usuário";
        $dados['subtitulo'] = "Alterar dados";
        $dados['url'] = 'admin/alterar_usuario';
        
        $this->load->model('usuarios_model', 'modelusuarios');
        $dados['usuario'] = $this->modelusuarios->alterar($id);
        
        $this->load->view('admin/template/index', $dados);
        

    }
   
}