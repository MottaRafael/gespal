<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
 
	public function index(){
		$this->load->view('admin/login');
	}
	
	public function logar(){
            
            $this->load->library('form_validation');// CARREGA O FORM_VALIDATION
            $this->form_validation->set_rules('usuario', 'Usuário', 'required|min_length[3]');//INPUT USUARIO NA VIEW LOGIN É REQUERIDO
            $this->form_validation->set_rules('senha', 'Senha', 'required|min_length[3]');//INPUT USUARIO NA VIEW LOGIN É REQUERIDO

            $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
            $userIp=$this->input->ip_address();
            $secret = $this->config->item('google_secret');  
            $url="https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$recaptchaResponse."&remoteip=".$userIp;
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url); 
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = curl_exec($ch); 
            curl_close($ch);
            $status = json_decode($output, true);
            
            if($this->form_validation->run() == FALSE){
                redirect(base_url('admin/login'));
            } elseif (($this->form_validation->run() == TRUE) && ($status['success'])) {
                
                $usuario = $this->input->post("usuario");//PEGA VALOR DO INPUT
                $senha = $this->input->post("senha");//PEGA VALOR DO INPUT
                $this->db->select('COD_USUARIO, NOME, RAMAL');
                $this->db->where('NOME', $usuario);//SELECT NO BANCO(CONDIÇÃO)
                $this->db->where('SENHA', $senha);//SELECT NO BANCO(CONDIÇÃO)
                
                $userLogado = $this->db->get('USUARIOS')->result();//SELECT NO BANCO(TABELA)
                if (count($userLogado) == 1) {//VERIFICA E SETA "LOGADO == 1" 
                    foreach ($userLogado as $dados_user){
                        
                    $array = array(
                                   "logado" => 1,
                                   "codigo" => $dados_user->COD_USUARIO,
                                   "nome"   => $dados_user->NOME,
                                   "ramal"  => $dados_user->RAMAL
                                   );
                    }
                    
                    $this->session->set_userdata($array);
                    
                    redirect(base_url());//CARREGA INDEX
                    
                }else{
                    
                   $this->load->view("admin/login"); //RETORNA PRO LOGIN              
                }
                
            } else {
                $this->load->view("admin/login"); //RETORNA PRO LOGIN    
            }            
	}
        
	public function logout(){
		$this->session->unset_userdata("logado"); //REMOVE STATUS "LOGADO"
		redirect(base_url('admin/login'));//RETORNA PRO LOGIN   
	}
        
        public function usuarios_json(){
            
            if(isset($_POST['letras'])){
                $nome_usuario = $_POST['letras'];
            
                $usuarios = $this->db->query("select NOME "
                        . "from USUARIOS "
                        . "where lower(NOME) "
                        . "like '%" . strtolower($nome_usuario) . "%' "
                        . "and ATIVO='S'");

                    foreach ($usuarios->result() as $usuario){
                    $dados[] = array(
                            'name' => trim($usuario -> NOME)
                    );
                }
                echo json_encode($dados);
            }
            
    }
	
}