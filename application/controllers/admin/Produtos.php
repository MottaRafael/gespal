<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Produtos extends CI_Controller {

    function __construct(){
        
        parent::__construct();
        $logado = $this->session->userdata("logado");
	if ($logado != 1) redirect(base_url('admin/login'));	
        
    }

    function index() {
        
        $dados['titulo'] = "Produtos";
        $dados['subtitulo'] = "Filtrar";
        $dados['url'] = 'admin/produtos';
        
        $this->load->view('admin/template/index', $dados);
        

    }
    
}