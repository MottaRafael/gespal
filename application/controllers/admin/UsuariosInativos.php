<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class UsuariosInativos extends CI_Controller {

    function __construct(){
        
        parent::__construct();
        $logado = $this->session->userdata("logado");
	if ($logado != 1) redirect(base_url('admin/login'));	
        
    }

    function index() {
        $dados['titulo'] = "Usuários Inativos";
        $dados['subtitulo'] = "Filtrar";
        $dados['url'] = 'admin/usuarios_inativos';
        
        $this->load->view('admin/template/index', $dados);
    }
    
}