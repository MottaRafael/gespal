<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Permissoes extends CI_Controller {

    function __construct(){
        
        parent::__construct();
        $logado = $this->session->userdata("logado");
	if ($logado != 1) redirect(base_url('admin/login'));	
        
    }

    function index() {
        
        $dados['titulo'] = "Permissoes";
        $dados['subtitulo'] = "Filtrar";
        $dados['url'] = 'admin/permissoes';
       
      $dados['usuarios'] = $this->db->select('NOME, COD_USUARIO, RAMAL, ATIVO')
                         ->from('USUARIOS')
                         ->where('COD_USUARIO != ', 0)
                         ->order_by('ATIVO', 'DESC')
                         ->order_by('NOME', 'ACS')
                         ->get()
                         ->result();

      $this->load->view('admin/template/index', $dados);
        

    }
    
    function alterar($id) {

    $dados['titulo'] = "Permissoes Usuários";
    $dados['subtitulo'] = "Filtrar";
    $dados['url'] = 'admin/permissoes_usuarios';

      $dados['usuarios'] = $this->db->select('NOME, COD_USUARIO, RAMAL, ATIVO, NOME_COMPLETO')
                         ->from('USUARIOS')
                         ->where('COD_USUARIO != ', 0)
                         ->order_by('ATIVO', 'DESC')
                         ->where('COD_USUARIO', $id)
                         ->get()
                         ->result();

      $this->load->view('admin/template/index', $dados);
        
    }
    
    public function permissoesExcluir(){
         $idUser = $this->input->post('idUsuario');
         $idMenu = $this->input->post('idMenu');
         $this->load->model('usuarios_model', 'modelusuarios');
         $this->modelusuarios->permissoesExcluir($idUser, $idMenu);
    }
    
    public function permissoesAdicionar(){
       $idUser = $this->input->post('idUsuario');
       $idMenu = $this->input->post('idMenu');
       $this->load->model('usuarios_model', 'modelusuarios');
       $this->modelusuarios->permissoesAdicionar($idUser, $idMenu);
    }
    
}