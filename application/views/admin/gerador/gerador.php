<?php
$atributos = array('id'     => 'form_gerador',
                   'name'   => 'form_gerador');
echo form_open('admin/gerador/gerar_crud', $atributos);
?>
<style>
    .zoom {
            overflow: hidden;
    }

    .zoom #lazy {
            max-width: 100%;
            -moz-transition: all 0.3s;
            -webkit-transition: all 0.3s;
            transition: all 0.3s;
    }

    .zoom:hover #lazy {
            -moz-transform: scale(1.3);
            -webkit-transform: scale(1.3);
            transform: scale(1.3);
    }

    .lazy {
            position: absolute;
            left: 5%;
            right: 5%;
            bottom: 20px;
            z-index: 10;
            padding-top: 20px;
            padding-bottom: 20px;
            color: #ffffff;
            text-align: center;
            font-weight: 700;
            text-shadow: 0 1px 3px rgba(0,0,0,0.6);
    }
</style>
<script src="<?php echo base_url('public/js/jquery.min.js') ?>"></script>
    <div class="col-xs-12">
            <div class="col-xs-5">
                <label>Tabelas</label>
                <select name="tabela_db" id="tabela_db" class="form-control">
                        <?php 
                            foreach($tabelas as $tabela){
                                echo '<option value="'.$tabela->TABELA.'">'.$tabela->TABELA.'</option>';
                            }
                        ?>
                </select>
            </div>
            <div class="col-xs-2">
                <label>Model</label>
                <input type="text" value="" name="model" id="model" class="form-control" placeholder="Nome s/ _model" required/>
            </div>
            <div class="col-xs-2">
                <label>View</label>
                <input type="text" value="" name="view" id="view" class="form-control" placeholder="Nome view" required/>
            </div>
            <div class="col-xs-2">
                <label>Controller</label>
                <input type="text" value="" name="controller" id="controller" class="form-control" placeholder="Nome Controller" required/>
            </div>
            <div class="col-xs-5">
                    <br/><br/>
                    <label>Colunas: <span data-toggle="tooltip" class="fa fa-info-circle" title="Segure Ctrl para selecionar mais colunas"></span></label> <span id="span"></span>
                    <select multiple id="colunas-drop" name="colunas_db[]" style="height: 150px" class="form-control">
                    </select>
                    <hr/>
                <div class="col-xs-2" style="margin-left: 20%;">
                    <label for="select" style="font-weight: bold">Select</label>
                    <input type="checkbox" value="select" name="select" id="select" class="form-check-input" style="float: right" checked/>
                    <br/>
                    <label for="insert" style="font-weight: bold">Insert</label>
                    <input type="checkbox" value="insert" name="insert" id="insert" class="form-check-input" style="float: right"/>
                    <br/>
                    <label for="update" style="font-weight: bold">Update</label>
                    <input type="checkbox" value="update" name="update" id="update" class="form-check-input" style="float: right"/>
                    <br/>
                    <label for="delete" style="font-weight: bold">Delete</label>
                    <input type="checkbox" value="delete" name="delete" id="delete" class="form-check-input" style="float: right"/>   
                </div>
            </div>
        
            <div class="col-xs-6">
                <hr/>
                <label>Menu</label>
                <select class="form-control" name="menus" id="menus" required>
                    <?php 
                    foreach($menus as $menu){
                        if(strlen($menu->INDICE) == 2){
                            echo '<option value="'.$menu->INDICE.'">'.$menu->DESCRICAO.'</option>';
                        }
                    }
                    ?>
                </select>
                <br/>
                <label>Submenus: </label>
                <select id="submenu" name="submenu" class="form-control">
                </select>
            </div>
            <div class="col-xs-3">
                <hr/>
                <label>Nome do Menu: </label>
                <input type="text" name="nome_do_menu" id="nome_do_menu" class="form-control" required/>
            </div>
            <br/>
            <div class="col-xs-2">
                <hr/>
                <button type="button" class="btn btn-default" style="width: 110px; margin-top: 14.5px; margin-right: 15px;font-size:20px;" data-toggle="modal" data-target="#exampleModal">
                    Icone
                    <i style="font-size:25px;" name="icon" id="icon"></i>
                </button>
            </div>
            <input type="hidden" name="post_icone" id="post_icone" value=""/>
            <input type="hidden" name="descricao" id="descricao" value=""/>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"  aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" style="width: 90%;height: 90%;overflow-y: auto" role="document">
                        <div class="modal-content" style="overflow-y: auto">
                            <br/>
                            <div class="col-xs-12">
                                <label style="margin-left: 20%;font-weight: bold">Buscar Icones</label>
                                <input type="text" id="txtBusca" name="txtBusca" value="" class="form-control"style="margin: auto;width: 60%;border: 1px solid #000000;padding: 10px;" placeholder="Ex: fa-book"/>
                            </div>
                            <div class="modal-body" style="margin-top: 5%">
                                <?php
                                $i = 0;
                                foreach($icones as $icone){
                                    echo '<div class="zoom col-xs-3">'
                                                . '<div class="col-xs-12" id="icones'.$i.'" data-dismiss="modal" style="margin: 1%">'
                                                        . '<div class="icone">'
                                                                . '<input type="hidden" value="fa ' . $icone . '" name="icone2'.$i.'" id="icone2'.$i.'"/>'
                                                                . '<i style="font-size:35px" class="fa ' . $icone . '" id="lazy"></i> ' . $icone . ''
                                                        . '</div>'
                                                . '</div>'
                                       . '</div>';
                                    
                                    echo '<script>'
                                       . '$("#icones'.$i.'").click(function(){
                                             $("#icon").removeClass();
                                             var icone'.$i.' = $("#icone2'.$i.'").val();
                                             $("#post_icone").val(icone'.$i.');
                                             $("#icon").addClass(icone'.$i.');
                                          });'
                                       . '</script>';
                                    
                                    $i++;
                                    
                                }
                                ?>
                            </div>
                            <div class="modal-footer">
                                <!--<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>-->
                            </div>
                        </div>
                    </div>
                </div>
        
    </div>

        <div class="col-xs-12">
            <div class="col-xs-5"></div>
            
            <div class="col-xs-1">
                <br/><hr/>
                <button class="btn btn-warning" style="width: 150px">Gerar</button>
                <br/><hr/>
            </div>
        </div>
<?php 
echo form_close();
?>


<script>

    let dropdown = $('#colunas-drop');
    dropdown.empty();
    dropdown.append('<option selected="true" disabled>Escolha as colunas</option>');
    dropdown.prop('selectedIndex', 0);
    
    let dropdown2 = $('#submenu');
    dropdown2.empty();
    dropdown2.append('<option selected="true" disabled>Escolha o submenu</option>');
    dropdown2.prop('selectedIndex', 0);

    $("#tabela_db").change(function(){
    var data = $("#tabela_db").val();
    var texto = data.replace(/^\s+|\s+$/g,"");

    $.ajax({
      type: "POST",
      url: "gerador/colunas_db",
      data: {"tabela_db" : texto},
      dataType: "json",
      success: function(data) {
            $("#colunas-drop").empty();
            $("#span").empty();
            $.each(data, function (key, entry) {
              dropdown.append($('<option></option>').attr('value', entry.COLUNAS).text(entry.COLUNAS));
            })
      }
    });
        return false;
    });
    
    $("#menus").change(function(){
    var data = $("#menus").val();

    $.ajax({
      type: "POST",
      url: "gerador/itens_menu",
      data: {"menus" : data},
      dataType: "json",
      success: function(data) {
            $("#submenu").empty();
            $.each(data, function (key, entry) {
                  dropdown2.append($('<option></option>').attr('value', entry.INDICE).text(entry.DESCRICAO));
            })
      }
    });
        return false;
    });
    
   $("#colunas-drop").click(function(){
        var span = $("#colunas-drop").val();
        $("#span").text(span);
   });
      
   $(function(){
      $("#txtBusca").keyup(function(){
        var texto = $(this).val();
        $(".icone").each(function(){
          var resultado = $(this).text().toUpperCase().indexOf(' '+texto.toUpperCase());
          if(resultado < 0) {
            $(this).fadeOut();
          }else {
            $(this).fadeIn();
          }
        }); 
      });
    });
    
    

</script>