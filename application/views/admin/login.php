<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Gestão Palterm | Login</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url('public/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/dist/font-awesome/css/font-awesome.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/dist/Ionicons/css/ionicons.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/css/AdminLTE.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/css/skin-blue.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/css/easy-autocomplete.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/css/easy-autocomplete.themes.min.css') ?>">
  <link rel="shortcut icon" href="<?php echo base_url('public/img/logo/logo.png') ?>" type="image/x-icon" />
  
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat">
  <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body class="hold-transition login-page">

    <div class="login-box">
        <div class="login-logo">
            <a href="http://palterm.com.br" class="logo">
                <span class="logo-mini"><b><img style="width: 80px;height: 80px" src="<?php echo base_url('public/img/logo/logo.png') ?>"/></span>
  
      <span class="logo-lg"><font style="font-weight: bold">GES</font>TÃO<font style="font-weight: bold"> PAL</font>TERM</span>
            </a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Logar</p>
            <?php 
            
            echo validation_errors('<div class="alert alert-warning">','</div>'); 
            $atributos = array('id'   => 'form_login',
                               'name' => 'form_login');
            echo form_open('admin/login/logar', $atributos);
            
            ?>
                <div class="form-group has-feedback">
                    <input type="text" id="usuario" name="usuario" class="form-control" placeholder="Usuario" required autofocus>
                    <span class="fa fa-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" id="senha" name="senha" class="form-control" placeholder="Senha" required>
                    <input type="hidden" name="g-recaptcha-response" id="g-recaptcha-response">
                    <span class="fa fa-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-4"></div>
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Entrar</button>
                    </div>
                </div>
           <?php echo form_close(); ?>
        </div>
    </div>

<script src="<?php echo base_url('public/js/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('public/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('public/dist/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<script src="<?php echo base_url('public/dist/fastclick/lib/fastclick.js') ?>"></script>
<script src="<?php echo base_url('public/js/adminlte.min.js') ?>"></script>
<script src="<?php echo base_url('public/js/demo.js') ?>"></script>
<script src="<?php echo base_url('public/js/jquery.easy-autocomplete.min.js') ?>"></script>

<script src="https://www.google.com/recaptcha/api.js?render=6Leh6IIUAAAAABfLH1cyqxfKvaeKG7GVxhzlyq22"></script>
<script>

    var usuarios={
            url: function(letras){return "login/usuarios_json";},
            getValue: function(element){return element.name;},
            list:{
              onSelectItemEvent: function(){
                    },
            onHideListEvent: function(){}
            },
            ajaxSettings:{
                dataType: "json", 
                method: "POST", 
            data:{
                dataType: "json"
            }},
            preparePostData: function(data){
                data.letras = $("#usuario").val();
                return data;
            },
            requestDelay: 400
        };

$("#usuario").easyAutocomplete(usuarios);

grecaptcha.ready(function() {
    grecaptcha.execute('6Leh6IIUAAAAABfLH1cyqxfKvaeKG7GVxhzlyq22', {action: 'action_name'}).then(function(token) {
        document.getElementById('g-recaptcha-response').value=token;
    });
});

</script>
</body>
</html>
