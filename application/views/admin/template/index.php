<?php
date_default_timezone_set('America/Sao_Paulo');
ob_start();
$mpdf = new \Mpdf\Mpdf();
//echo "<pre>";
//die(var_dump($menus));
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Gestão Palterm | Gespal</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url('public/css/bootstrap.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/dist/font-awesome/css/font-awesome.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/dist/Ionicons/css/ionicons.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/css/AdminLTE.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/css/skin-blue.min.css') ?>">
  <link rel="stylesheet" href="<?php echo base_url('public/css/datatables.min.css') ?>"/>
  <link rel="stylesheet" href="<?php echo base_url('public/css/morris.css') ?>"/>
  <link rel="stylesheet" href="<?php echo base_url('public/css/jquery-ui.css') ?>"/>
  <link rel="shortcut icon" href="<?php echo base_url('public/img/logo/logo.png') ?>" type="image/x-icon" /> 
  
  <style>
      .div_nome_usuario{
          margin-top: -1%;
      }
        input[type=number]::-webkit-outer-spin-button,
        input[type=number]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        input[type=number] {
            -moz-appearance:textfield;
        }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
    <header class="main-header">
    <a href="#" class="logo">
      <span class="logo-mini"><b><img style="width: 50px;height: 50px" src="<?php echo base_url('public/img/logo/logo.png') ?>"/></span>
      <span class="logo-lg"><font style="font-weight: bold">GES</font>TÃO<font style="font-weight: bold"> PAL</font>TERM</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="hidden-xs">
                  <i class="fa fa-user" aria-hidden="true"></i>

                <?php 
                    $nome_usuario = $this->session->userdata('nome');
                    $cod_usuario = $this->session->userdata('codigo');
                    echo $nome_usuario;
                ?>
              </span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <p>
                 <?php 
                    $nome_usuario = $this->session->userdata('nome');
                    echo $nome_usuario;
                ?>
                </p>
                <i class="fa fa-user" style="font-size: 100px;" aria-hidden="true"></i>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url('admin/usuarios/alterar/'.$cod_usuario) ?>" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url('admin/login/logout'); ?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out" aria-hidden="true"></i> Sair</a>
                </div>
              </li>
            </ul>
          </li>
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

<aside class="main-sidebar">
    <section class="sidebar">
        
      <ul class="sidebar-menu">
        <li class="header">OPÇÕES</li>
        <li class="active">
          <a href="<?php echo base_url() ?>"><i class="fa fa-line-chart" aria-hidden="true"></i> <span>Dashboard</span></a>
        </li>
				
<?php
        $tamanho_indice = 0;
        $tamanho_indice_anterior = 0;
        
        
        $menus2 = $this->db->query("select itens_menu.nome,itens_menu.icone,itens_menu.nome_arquivo,itens_menu.indice from usuarios_permissoes_menus
                        inner join itens_menu on (usuarios_permissoes_menus.cod_menu = itens_menu.id)
                        where usuarios_permissoes_menus.pode_acessar = 'S' and usuarios_permissoes_menus.cod_usuario = " . $cod_usuario . " order by itens_menu.indice")
                          ->result();

            foreach ($menus2 as $campoMenu){
                if(trim($campoMenu->NOME_ARQUIVO) == null || ""){
                    $arquivo2 = base_url();
                }else{
                    $arquivo2 = base_url("$campoMenu->NOME_ARQUIVO");
                }
                
                
                $arquivo = $campoMenu->NOME_ARQUIVO <> "" ? "onclick=\"abrirMenu('".$campoMenu->NOME_ARQUIVO."')\"" : "";
                
                $tamanho_indice = strlen($campoMenu->INDICE);
                
                if ($tamanho_indice == 2){ //nivel 1
                        if ($tamanho_indice_anterior >= 8)echo "</li>";
                        if ($tamanho_indice_anterior >= 8)echo "</ul>";
                        if ($tamanho_indice_anterior >= 6)echo "</li>";
                        if ($tamanho_indice_anterior >= 6)echo "</ul>";
                        if ($tamanho_indice_anterior >= 4)echo "</li>";
                        if ($tamanho_indice_anterior >= 4)echo "</ul>";
                        if ($tamanho_indice_anterior >= 2)echo "</li>";
                        echo '<li class="treeview">';
                        echo '<a href="javascript:void(0)" '.$arquivo.'>';
                        echo '<i class="'.$campoMenu->ICONE.'"></i> <span>'.$campoMenu->NOME.'</span>';

                        if (substr($arquivo, 0, 7) != "onclick"){
                                echo '<i class="fa fa-angle-right pull-right"></i>';
                        }
                        echo '</a>';
                        
                }else if ($tamanho_indice == 4){ //nivel 2
                        if ($tamanho_indice_anterior == 8)echo "</li>";
                        if ($tamanho_indice_anterior == 8)echo "</ul>";
                        if ($tamanho_indice_anterior == 8)echo "</li>";
                        if ($tamanho_indice_anterior == 8)echo "</ul>";
                        if ($tamanho_indice_anterior == 8)echo "</li>";
                        if ($tamanho_indice_anterior == 6)echo "</li>";
                        if ($tamanho_indice_anterior == 6)echo "</ul>";
                        if ($tamanho_indice_anterior == 4)echo "</li>";
                        if ($tamanho_indice_anterior < 4)echo '<ul class="treeview-menu">';
                        echo '<li class="treeview">';
                        echo '<a href="javascript:void(0)" '.$arquivo.'>';
                        echo '<i class="'.$campoMenu->ICONE.'"></i> <span>'.$campoMenu->NOME.'</span>';

                        if (substr($arquivo, 0, 7) != "onclick"){
                                echo '<i class="fa fa-angle-right pull-right"></i>';
                        }
                        
                        echo '</a>';

                }else if ($tamanho_indice == 6){ //nivel 3
                        if ($tamanho_indice_anterior == 8)echo "</li>";
                        if ($tamanho_indice_anterior == 8)echo "</ul>";
                        if ($tamanho_indice_anterior == 6)echo "</li>";
                        if ($tamanho_indice_anterior < 6)echo '<ul class="treeview-menu">';
                        echo '<li class="treeview">';
                        echo '<a href="javascript:void(0)" '.$arquivo.'>';
                        echo '<i class="'.$campoMenu->ICONE.'"></i> <span>'.$campoMenu->NOME.'</span>';
                        
                        if (substr($arquivo, 0, 7) != "onclick"){
                                echo '<i class="fa fa-angle-right pull-right"></i>';
                        }
                        
                        echo '</a>';
                        
                }else if ($tamanho_indice == 8){ //nivel 4
                        if ($tamanho_indice_anterior == 8)echo "</li>";
                        if ($tamanho_indice_anterior < 8)echo '<ul class="treeview-menu">';
                        echo '<li class="treeview">';
                        echo '<a href="javascript:void(0)" '.$arquivo.'>';
                        echo '<i class="'.$campoMenu->ICONE.'"></i> <span>'.$campoMenu->NOME.'</span>';
                        
                        if (substr($arquivo, 0, 7) != "onclick"){
                                echo '<i class="fa fa-angle-right pull-right"></i>';
                        }
                        
                        echo '</a>';
                }
                $tamanho_indice_anterior = $tamanho_indice;
            }
                if ($tamanho_indice_anterior >= 8){
                        echo "</li>";
                        echo "</ul>";
                }
                if ($tamanho_indice_anterior >= 6){
                        echo "</li>";
                        echo "</ul>";
                }
                if ($tamanho_indice_anterior >= 4){
                        echo "</li>";
                        echo "</ul>";
                }
                if ($tamanho_indice_anterior >= 2){
                        echo "</li>";
                }
?>

        </ul>
    </section>
</aside>
    
  <div class="content-wrapper">
    <section class="content-header">
      <h3>
          <div class="div_nome_usuario">
               <?php 
                if(isset($titulo))echo $titulo; 
              ?>     
          </div>
      </h3>
    </section>

    <section class="content">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
              
              <?php 
                    if(isset($subtitulo)) echo $subtitulo;
              ?>
              
          </h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
            <?php
            if(isset($url)){
               $this->load->view($url, $subtitulo);
            }
            ?>
        </div>
      </div>
    </section>
      
      
      
  </div>
				
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Versão</b> 1.0
    </div>
    <strong> Todos os direitos reservados <a href="http://palterm.com.br/">Palterm Company</a> &copy; 2019.</strong>
</footer>
        
        <aside class="control-sidebar control-sidebar-dark">
            <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
            </ul>
            <div class="tab-content">
                        <h3 class="control-sidebar-heading">Opções Gerais</h3>

                        <div class="form-group">
                                <label class="control-sidebar-subheading">
                                        Permissões Sistema
                                        <a href="<?php echo base_url('admin/permissoes') ?>" class="text-red pull-right"><i class="fa fa-list"></i></a>
                                </label>
                        </div>
            </div>
        </aside>
</div>
    
<script src="<?php echo base_url('public/js/jquery.min.js') ?>"></script>
<script src="<?php echo base_url('public/js/jquery-ui.js') ?>"></script>
<script src="<?php echo base_url('public/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('public/dist/jquery-slimscroll/jquery.slimscroll.min.js') ?>"></script>
<script src="<?php echo base_url('public/dist/fastclick/lib/fastclick.js') ?>"></script>
<script src="<?php echo base_url('public/js/adminlte.min.js') ?>"></script>
<script src="<?php echo base_url('public/js/morris.min.js') ?>"></script>
<script src="<?php echo base_url('public/js/datatables.min.js') ?>"></script>
<script src="<?php echo base_url('public/js/dataTables.buttons.min.js') ?>"></script>
<script src="<?php echo base_url('public/js/buttons.flash.min.js') ?>"></script>
<script src="<?php echo base_url('public/js/jszip.min.js') ?>"></script>
<script src="<?php echo base_url('public/js/pdfmake.min.js') ?>"></script>
<script src="<?php echo base_url('public/js/vfs_fonts.js') ?>"></script>
<script src="<?php echo base_url('public/js/buttons.html5.min.js') ?>"></script>
<script src="<?php echo base_url('public/js/buttons.print.min.js') ?>"></script>
<script src="<?php echo base_url('public/js/jquery.tristate.js') ?>"></script>
<script src="<?php echo base_url('public/js/demo.js') ?>"></script>
<script src="<?php echo base_url('public/js/functions.js') ?>"></script>

<script>
    
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  
      });
      function abrirMenu(arquivo){
	window.location.href = "<?php echo base_url() ?>"+arquivo;

      }

</script>

</body>
</html>
