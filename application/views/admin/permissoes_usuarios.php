<?php

    foreach($usuarios as $usuario){

?>

<style>
.container {
    position: relative;
    width: 50%;
}
</style>

<section class="content-header">
	<h1>Perfil de Usuário</h1>
</section>

<section class="content">
	<div class="row">
		<div class="col-md-4">
			<div class="box box-primary">
				<div class="box-body">
					<center>
						<div class="container">
							<div class="userProfile image" nomeCompleto="<?php echo $usuario->NOME; ?>"></div>
						</div>
					</center>
					<h3 class="profile-username text-center"><?php echo $usuario->NOME;?></h3>
					<p class="text-muted text-center">FUNÇÃO USUÁRIO</p>
				</div>
			</div>

			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Sobre...</h3>
				</div>

				<div class="box-body">
					<strong><i class="fa fa-book margin-r-5"></i> Colaborador desde:</strong>
					<p class="text-muted">
						DATA INICIO EMPRESA
					</p>
					<hr>
					<strong><i class="fa fa-birthday-cake margin-r-5"></i> Aniversário:</strong>
					<p class="text-muted">DATA ANIVER</p>
					<hr>
					<strong><i class="fa fa-phone margin-r-5"></i> Ramal:</strong>
					<p class="text-muted"><?php echo $usuario->RAMAL;?></p>
				</div>
			</div>
		</div>

        <div class="col-md-8">
			<div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
					<li class="active"><a href="#general" data-toggle="tab">Opções</a></li>
					<li ><a href="#menus" data-toggle="tab">Permissões de Menus</a></li>
				</ul>
				<div class="tab-content">
					<!--TAB OPCOES GERAIS-->
					<div class="active tab-pane" id="general">
						<form class="form-horizontal">
							<div class="form-group">
								<label for="perfilNomeCompleto" class="col-sm-2 control-label">Nome Completo</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="perfilNomeCompleto" placeholder="Nome Completo" value="<?php echo $usuario->NOME_COMPLETO;?>">
								</div>
							</div>
							<div class="form-group">
								<label for="perfilRamal" class="col-sm-2 control-label">Ramal</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="perfilRamal" placeholder="Ramal" value="<?php echo $usuario->RAMAL;?>">
								</div>
							</div>
							<div class="form-group">
								<label for="perfilUsuario" class="col-sm-2 control-label">Nome de Usuário</label>
								<div class="col-sm-10">
									<input type="text" class="form-control" id="perfilUsuario" placeholder="Nome de Usuário" value="<?php echo $usuario->NOME;?>" disabled>
								</div>
							</div>
						</form>
					</div>

					
					<!--TAB PERMISSOES MENUS-->
					<div class="tab-pane" id="menus">
						<form class="form-horizontal">
							<?php
							$consultaMenus = $this->db->query("select id,descricao from itens_menu order by indice")->result();
                                                        
							foreach ($consultaMenus as $campoMenu){
                                                            
								if($consultaPermissoes = $this->db->query("select pode_acessar from usuarios_permissoes_menus where cod_usuario=".trim($usuario->COD_USUARIO)." and cod_menu=".$campoMenu->ID)->row()){
									
									$permiteAcesso = true;
								}else{
									$permiteAcesso = false;
								}
								?>
								<div class="checkbox">
									<label><input <?php echo ($permiteAcesso==true ? 'checked' : 'unchecked')?> class="checkboxMenus" type="checkbox" id="checkboxMenu<?php echo $campoMenu->ID; ?>" value="<?php echo $campoMenu->ID; ?>"><?php echo str_replace(" ", "&nbsp;&nbsp;", $campoMenu->DESCRICAO) ?>
									</label>
								</div>
							<?php							
								}
							?>
						</form>
					</div>
					<!--TAB PERMISSOES MENUS-->					
				</div>
			</div>
		</div>
	</div>
</section>
<script src="<?php echo base_url('public/js/jquery.min.js') ?>"></script>
<script>
	
	
	

	$(".checkboxMenus").click(function(){
		if($(this).is(':checked')){
			$.ajax({
                            type: "POST",
                            url: "<?php echo base_url('admin/permissoes/permissoesAdicionar') ?>",
                            data: {"idUsuario" : <?php echo trim($usuario->COD_USUARIO);?>,
                                   "idMenu" : $(this).val()},
                            dataType: "json",
			}, function(retorno){
//				if(retorno!='ok'){
					alert(retorno);
//				}
			});
		}else{
			$.ajax({
                            type: "POST",
                            url: "<?php echo base_url('admin/permissoes/permissoesExcluir') ?>",
                            data: {"idUsuario" : <?php echo trim($usuario->COD_USUARIO);?>,
                                   "idMenu" : $(this).val()},
                            dataType: "json",
			}, function(retorno){
//				if(retorno!='ok'){
					alert(retorno);
//				}
			});
		}
	});
</script>

<?php
        }
//}

?>