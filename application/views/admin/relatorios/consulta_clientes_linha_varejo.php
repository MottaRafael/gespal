<?php
        ini_set("memory_limit", "512M");
        ini_set("pcre.backtrack_limit", "5000000");
        
        $total_clientes = 0;
        $i=0;
	$html = '<table width="100%" class="bpmTopic" style="overflow: hidden">';
           
            '<thead>';
            
                $resultado = array();
                foreach ($linhas_varejos as $linha_varejo){
                    $resultado[$linha_varejo->COD_LINHA_VAREJO][] = $linha_varejo;
                }
                
                foreach ($resultado as $chave => $valor){
                    
                    $sql = $this->db->query('SELECT LINHA_VAREJO FROM LINHA_VAREJO WHERE COD_LINHA_VAREJO = '.$chave);
                    $linha = $sql->row();
                    
                    $html .= '<tr>'
                            . '<td colspan="8" id="fantasia">Linha Varejo:  <b>'.$chave." - " . $linha->LINHA_VAREJO . '</b></td>'
                            . '</tr>';
                      
                    $html .= '<tr class="headerrow">
                            <td align="left"><u>Cód.</u></td>
                            <td align="left"><u>Razão</u></td>
                            <td align="left"><u>Grupo Cad.</u></td>
                            <td align="left"><u>CNPJ</u></td>
                            <td align="left"><u>Cidade</u></td>
                            <td align="left"><u>UF</u></td>
                            <td align="left"><u>Bairro</u></td>
                            <td align="left"><u>Repres.</u></td>
                            <td align="left"><u>Vendedor</u></td>
                            <td align="right"><u>Grupo Rede</u></td>    
                    </tr>';
                    $i = 0;
                    foreach ($valor as $c => $v){
                        $html .= '</thead><tbody>';
               
                        $html .= '<tr>'
                            . '<td>'.$v->CODIGO.'</td>
                            <td style="white-space: nowrap; overflow: hidden;">'.$v->NOME_CLIENTE.'</td>
                            <td style="white-space: nowrap; overflow: hidden;" align="left">'.$v->COD_GRUPO. ' - '. $v->DESCRICAO .'</td>
                            <td style="white-space: nowrap; overflow: hidden;" align="left">'.$v->CGC.'</td>
                            <td style="white-space: nowrap; overflow: hidden;" align="left">'.$v->CIDADEFATURAMENTO.'</td>
                            <td style="white-space: nowrap; overflow: hidden;" align="left">'.$v->UFFATURAMENTO.'</td>
                            <td style="white-space: nowrap; overflow: hidden;" align="left">'.$v->BAIRROFATURAMENTO.'</td>
                            <td style="white-space: nowrap; overflow: hidden;" align="left">'.$v->NOME_REPRES.'</td>
                            <td style="white-space: nowrap; overflow: hidden;" align="left">'.$v->NOME_VEND.'</td>
                            <td style="white-space: nowrap; overflow: hidden;" align="right">'.$v->GRUPO_REDE.'</td>'
                            . '</tr>';
                       
                        $total_clientes++;
                        $i++;
                        
                    }
                   $html .= '<tr class="separated">
                                  <td bgcolor="#b3b3b3">
                                          <b>#</b>
                                  </td>
                                  <td bgcolor="#b3b3b3" colspan="7">
                                          <b>TOTAL DO GRUPO : ' . number_format($i, 0, ',', '.') . '</b>
                                  </td>
                                  <td align="right" colspan="2"  bgcolor="#b3b3b3">
                                  </td>
                    </tr>';
                }
                
                $html .= '<tr>
                       <td bgcolor="#b3b3b3">
                               <b>#</b>
                       </td>
                       <td bgcolor="#b3b3b3" colspan="6">
                               <b>TOTAL GERAL DE CLIENTES</b>
                       </td>
                       <td align="right" bgcolor="#b3b3b3" colspan="3">
                               <b>' . number_format($total_clientes, 0, ',', '.') . '</b>
                       </td>
               </tr>';
                     
               $html .= '</tbody></table>';
     
        
        $header =  ('<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-size: 7pt;">'
                 . '<tr>'
                 . '<td width="33%"></td>'
                 . '<td width="33%" align="center"><img src="' . base_url() . 'public/img/logo/logorel.png" height="4%" /></td>'
                 . '<td width="33%" style="text-align: right;"><br></td>'
                 . '</tr>'
                 . '</table>');
        
        $footer =  ('<div style="border-top: 1px solid #000000; font-size: 8pt; text-align: justify; padding-top: 3mm; ">'
                 . 'Este documento pode conter informação confidencial ou privilegiada, '
                 . 'sendo seu sigilo protegido. Se você não for o destinatário ou a pessoa '
                 . 'autorizada a receber este documento, não pode usar, copiar ou divulgar '
                 . 'as informações nele contidos ou tomar qualquer ação baseada nessas informações. '
                 . 'Se você recebeu este documento por engano, por favor, destrua-o imediatamente.<br>'
                 . '<div style="font-size: 9pt; text-align: center; padding-top: 3mm; ">Página {PAGENO} de {nb}</div>'
                 . '</div>');
        
        
        $mpdf = new Mpdf\Mpdf();
        $mpdf->SetHTMLHeader($header);
        $mpdf->SetHTMLFooter($footer);
        $stylesheet = file_get_contents(base_url('public/css/mpdfstyletables.css'));
	$mpdf->writeHTML($stylesheet, 1);
        $mpdf->AddPage('L', // L - landscape, P - portrait 
        '', '', '', '',
        5, // margin_left
        5, // margin right
       20, // margin top
       25, // margin bottom
        5, // margin header
        5); // margin footer
	$mpdf->WriteHTML($html, 2);
	$mpdf->Output();
        exit();
        
?>