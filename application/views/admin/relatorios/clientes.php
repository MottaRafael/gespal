<?php

echo validation_errors('<div class="alert alert-warning">','</div>'); 
$atributos = array('id'     => 'form_relatorio_clientes',
                   'name'   => 'form_relatorio_clientes',
                   'target' => '_blank' );
echo form_open('admin/relatorios/clientes/gera_relatorio_clientes', $atributos);

 ?>
<div class="col-xs-7 thumbnail">
    <div class="row">
        <div class="col-xs-12">
                <div class="col-xs-3">
                    <label for="cliente_inicial">Cliente Inicial: </label>
                    <input type="number" value="" class="col-xs-12 form-control" name="cliente_inicial" id="cliente_inicial" />
                </div>
                <div class="col-xs-3">
                    <label for="cliente_final">Cliente Final: </label>
                    <input type="number" value="" class="col-xs-12 form-control" name="cliente_final" id="cliente_final" />
                </div>
                <div class="col-xs-2"></div>
                <div class="col-xs-4">
                    <label for="cnpj">CNPJ: </label>
                    <input type="text" value="" class="col-xs-12 form-control" name="cnpj" id="cnpj" />
                </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <br/>
            <div class="col-xs-6" id="div">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="grupo_cadastro_inicial">Grupo de cad. inicial: </label>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <input type="text" value="" name="grupo_cadastro_inicial_input" class="form-control" id="grupo_cadastro_inicial_input" />
                    </div>
                    <div class="col-xs-9">
                        <select class="form-control" name="grupo_cadastro_inicial" id="grupo_cadastro_inicial">
                            <option></option>
                                <?php
                                foreach ($grupos_clifor as $grupo_clifor){
                                   echo '<option  value="'.$grupo_clifor->COD_GRUPO.'">' .$grupo_clifor->DESCRICAO . '</option>';
                                }
                                ?>                                
                        </select>
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="grupo_cadastro_final">Grupo de cad. final: </label>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <input type="number" value="" name="grupo_cadastro_final_input" class="form-control" id="grupo_cadastro_final_input" />
                    </div>
                    <div class="col-xs-9">
                        <select value="" name="grupo_cadastro_final"  class="form-control" id="grupo_cadastro_final">
                            <option></option>
                            <?php
                                foreach ($grupos_clifor as $grupo_clifor){
                                   echo '<option value="'.$grupo_clifor->COD_GRUPO.'">' .$grupo_clifor->DESCRICAO . '</option>';
                                }
                              ?>  
                        </select>
                    </div>
                </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
        <br/>
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-12">
                        <label for="repres_inicial">Repres. inicial: </label>
                    </div>
                </div>
                <div class="col-xs-3">
                    <input type="number" value="" name="repres_inicial_input" class="form-control" id="repres_inicial_input" />
                </div>
                <div class="col-xs-9">
                    <select value="" name="repres_inicial_select" class="form-control" id="repres_inicial_select">
                        <option></option>
                        <?php
                            foreach ($representantes as $representante){
                               echo '<option value="'.$representante->CODIGO.'">' .$representante->NOME . '</option>';
                            } 
                        ?>  
                    </select>
                </div>
            </div>
        
            <div class="col-xs-6">
                <div class="row">
                    <div class="col-xs-12">
                        <label for="repres_final">Repres. final: </label>
                    </div>
                </div>
                <div class="col-xs-3">
                    <input type="number" value="" name="repres_final_input" class="form-control" id="repres_final_input" />
                </div>
                <div class="col-xs-9">
                    <select value="" name="repres_final_select"  class="form-control" id="repres_final_select">
                        <option></option>
                       <?php
                            foreach ($representantes as $representante){
                               echo '<option value="'.$representante->CODIGO.'">' .$representante->NOME . '</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <br/>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="vend_inicial">Vend. inicial: </label>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <input type="number" value="" name="vend_inicial_input" class="form-control" id="vend_inicial_input" />
                    </div>
                    <div class="col-xs-9">
                        <select value="" name="vend_inicial_select" class="form-control" id="vend_inicial_select">
                            <option></option>
                            <?php
                                foreach ($vendedores as $vendedor){
                                   echo '<option value="'.$vendedor->CODIGO.'">' .$vendedor->NOME . '</option>';
                                } 
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="vend_final">Vend. final: </label>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <input type="number" value="" name="vend_final_input" class="form-control" id="vend_final_input" />
                    </div>
                    <div class="col-xs-9">
                        <select value="" name="vend_final_select"  class="form-control" id="vend_final_select">
                            <option></option>
                            <?php
                                foreach ($vendedores as $vendedor){
                                   echo '<option value="'.$vendedor->CODIGO.'">' .$vendedor->NOME . '</option>';
                                } 
                            ?>
                        </select>
                    </div>
                </div>
        </div>
    </div>
    
    <div class="row">    
        <div class="col-xs-12">
            <br/>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="superv_inicial">Superv. inicial: </label>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <input type="number" value="" name="superv_inicial_input" class="form-control" id="superv_inicial_input" />
                    </div>
                    <div class="col-xs-9">
                        <select value="" name="superv_inicial_select" class="form-control" id="superv_inicial_select">
                            <option></option>
                            <?php
                                foreach ($supervisores as $supervisor){
                                   echo '<option value="'.$supervisor->CODIGO.'">' .$supervisor->NOME . '</option>';
                                } 
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="superv_final">Superv. final: </label>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <input type="number" value="" name="superv_final_input" class="form-control" id="superv_final_input" />
                    </div>
                    <div class="col-xs-9">
                        <select value="" name="superv_final_select"  class="form-control" id="superv_final_select">
                            <option></option>
                            <?php
                                foreach ($supervisores as $supervisor){
                                   echo '<option value="'.$supervisor->CODIGO.'">' .$supervisor->NOME . '</option>';
                                } 
                            ?>
                        </select>
                    </div>
                </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <br/>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="promotor">Promotor: </label>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <input type="number" value="" name="promotor_input" class="form-control" id="promotor_input" />
                    </div>
                    <div class="col-xs-9">
                        <select value="" name="promotor_select" class="form-control" id="promotor_select">
                            <option></option>
                            <?php
                                foreach ($promotores as $promotor){
                                   echo '<option value="'.$promotor->CODIGO.'">' .$promotor->NOME . '</option>';
                                } 
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="fantasia_contem">Fantasia contém: </label>
                        </div>
                    </div>
                    <input type="text" value="" name="fantasia_contem" class="form-control" id="fantasia_contem" />
                </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <br/>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="cidade_contem">Cidade contém: </label>
                        </div>
                    </div>
                    <input type="text" value="" name="cidade_contem" class="form-control" id="cidade_contem" />

                </div>

                <div class="col-xs-2">
                    <label for="uf">UF: </label>
                    <input type="text" value="" name="uf" class="form-control" id="uf" />
                </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <br/>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="rede_inicial">Rede inicial: </label>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <input type="number" value="" name="rede_inicial_input" class="form-control" id="rede_inicial_input" />
                    </div>
                    <div class="col-xs-9">
                        <select value="" name="rede_inicial_select" class="form-control" id="rede_inicial_select">
                            <option></option>
                            <?php
                                foreach ($grupos_rede as $grupo_rede){
                                   echo '<option value="'.$grupo_rede->COD_GRUPO_REDE.'">' .$grupo_rede->GRUPO_REDE . '</option>';
                                } 
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="rede_final">Rede final: </label>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <input type="number" value="" name="rede_final_input" class="form-control" id="rede_final_input" />
                    </div>
                    <div class="col-xs-9">
                        <select value="" name="rede_final_select"  class="form-control" id="rede_final_select">
                            <option></option>
                            <?php
                                foreach ($grupos_rede as $grupo_rede){
                                   echo '<option value="'.$grupo_rede->COD_GRUPO_REDE.'">' .$grupo_rede->GRUPO_REDE . '</option>';
                                } 
                            ?>
                        </select>
                    </div>
                </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <br/>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="carteira_input">Carteira: </label>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <input type="number" value="" name="carteira_input" class="form-control" id="carteira_input" />
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="shopping_contem">Shopping contém: </label>
                        </div>
                    </div>
                    <div class="col-xs-12 input-group">
                        <input type="text" value="" name="shopping_contem" class="form-control" id="shopping_contem" />
                        <span class="input-group-addon">
                            <a data-toggle="tooltip" data-placement="top" title="Para exibir clientes com qualquer informação escrita no campo Shopping, digite o sinal de asterisco *">?</a>
                        </span>
                    </div>
                </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <br/>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="">Data Cadastro: </label>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <input type="date" value="" name="data_cadastro_inicial" class="form-control" id="data_cadastro_inicial" />
                    </div>
                    <div class="col-xs-6">
                        <input type="date" value="" name="data_cadastro_final" class="form-control" id="data_cadastro_final" />
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="linha_varejo">Linha Varejo: </label>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <input type="number" value="" name="linha_varejo_input" class="form-control" id="linha_varejo_input" />
                    </div>
                    <div class="col-xs-9">
                        <select value="" name="linha_varejo_select"  class="form-control" id="linha_varejo_select">
                            <option></option>
                            <?php
                                foreach ($linhas_varejo as $linha_varejo){
                                   echo '<option value="'.$linha_varejo->COD_LINHA_VAREJO.'">' .$linha_varejo->LINHA_VAREJO . '</option>';
                                } 
                            ?>
                        </select>
                    </div>
                </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <br/>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="" style="color: red">Data Compra/ Contato: </label>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <input type="date" value="" name="data_compra_contato_inicial" class="form-control" id="data_compra_contato_inicial" />
                    </div>
                    <div class="col-xs-6">
                        <input type="date" value="" name="data_compra_contato_final" class="form-control" id="data_compra_contato_final" />
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="estatistica">Estatística: </label>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <select value="" name="estatistica"  class="form-control" id="estatistica">
                            <option></option>
                            <?php
                                foreach ($estatisticas as $estatistica){
                                   echo '<option name="estatistica" value="'.$estatistica->COD_SITUACAO.'">' .$estatistica->SITUACAO . '</option>';
                                } 
                            ?>
                        </select>
                    </div>
                </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <br/>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="">Data Reagend.: </label>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <input type="date" value="" name="data_reagenda_inicial" class="form-control" id="data_reagenda_inicial" />
                    </div>
                    <div class="col-xs-6">
                        <input type="date" value="" name="data_reagenda_final" class="form-control" id="data_reagenda_final" />
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="linha_mercado_">Linha de Mercado: </label>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <select value="" name="linha_mercado_"  class="form-control" id="linha_mercado_">
                            <option></option>
                            <?php
                                foreach ($linhas_mercado as $linha_mercado){
                                   echo '<option value="'.$linha_mercado->COD_LINHA_PRODUTOS.'">' .$linha_mercado->LINHA_PRODUTOS . '</option>';
                                } 
                            ?>
                        </select>
                    </div>
                </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <br/>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="">Data Aniversário: </label>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <input type="date" value="" name="data_aniversario_inicial" class="form-control" id="data_aniversario_inicial" />
                    </div>
                    <div class="col-xs-6">
                        <input type="date" value="" name="data_aniversario_final" class="form-control" id="data_aniversario_final" />
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="regiao_pais">Região País: </label>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <select value="" class="form-control" name="regiao_pais" id="regiao_pais">
                            <option></option>
                            <?php
                                foreach ($regioes_pais as $regiao_pais){
                                   echo '<option value="'.$regiao_pais->REGIAO_PAIS.'">' .$regiao_pais->REGIAO_PAIS . '</option>';
                                } 
                            ?>
                        </select>
                    </div>
                </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <br/>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="segmento">Segmento: </label>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <select value="" name="segmento"  class="form-control" id="segmento">
                            <option></option>
                            <?php
                                foreach ($segmentos as $segmento){
                                   echo '<option value="'.$segmento->COD_SEGMENTO.'">' .$segmento->SEGMENTO . '</option>';
                                } 
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="status">Status: </label>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <select value="" name="status"  class="form-control" id="status">
                            <option></option>
                            <?php
                                foreach ($status_geral as $status){
                                   echo '<option value="'.$status->COD_STATUS.'">' .$status->STATUS . '</option>';
                                } 
                            ?>
                        </select>
                    </div>
                </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <br/>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="">Data Sintegra: </label>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <input type="date" value="" name="data_sintegra_incial" class="form-control" id="data_sintegra_incial" />
                    </div>
                    <div class="col-xs-6">
                        <input type="date" value="" name="data_sintegra_final" class="form-control" id="data_sintegra_final" />
                    </div>
                </div>

                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="nome_contato_contem">Nome Contato Contém: </label>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <input type="text" value="" name="nome_contato_contem" class="form-control" id="nome_contato_contem" />
                    </div>
                </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-xs-12">
            <br/>
                <div class="col-xs-6">
                    <div class="row">
                        <div class="col-xs-12">
                            <label for="marca">Marca: </label>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <select value="" name="marca"  class="form-control" id="marca">
                            <option></option>
                            <?php
                                foreach ($marcas as $marca){
                                   echo '<option value="'.$marca->ID.'">' .$marca->DESCRICAO . '</option>';
                                }
                            ?>
                        </select>
                    </div>
                </div>
        </div>
    </div>
    
    <br/>
</div>

<div class="col-xs-1"></div>

<div class="col-xs-4 thumbnail">
    
    <div class="col-xs-6">
        <br/>
        <input type="radio" value="razao_radio" name="radio_button_clientes" id="razao_radio" checked/>
        <label for="razao_radio">Razão</label>
        <br/>
        <input type="radio" value="fantasia_radio" name="radio_button_clientes" id="fantasia_radio" />
        <label for="fantasia_radio">Fantasia</label>
        <br/>
        <input type="radio" value="carteira_radio" name="radio_button_clientes" id="carteira_radio" />
        <label for="carteira_radio">Carteira</label>
        <br/>
        <input type="radio" value="cidade_radio" name="radio_button_clientes" id="cidade_radio" />
        <label for="cidade_radio">Cidade</label>
        <br/>
        <input type="radio" value="cidade_carteira" name="radio_button_clientes" id="cidade_carteira" />
        <label for="cidade_carteira">Cidade/ Carteira</label>
        <br/>
        <input type="radio" value="uf_radio" name="radio_button_clientes" id="uf_radio" />
        <label for="uf_radio">UF</label>
        <br/>
        <input type="radio" value="rede_radio" name="radio_button_clientes" id="rede_radio" />
        <label for="rede_radio">Rede</label>
        <br/>
        <input type="radio" value="rede_sintetico_radio" name="radio_button_clientes" id="rede_sintetico" />
        <label for="rede_sintetico">Rede - Sintético</label>
        <br/>
        <input type="radio" value="linha_mercado_radio" name="radio_button_clientes" id="linha_mercado_radio" />
        <label for="linha_mercado_radio">Linha de Mercado</label>
        <br/>
        <input type="radio" value="linha_varejo_radio" name="radio_button_clientes" id="linha_varejo_radio" />
        <label for="linha_varejo_radio">Linha de Varejo</label>
        <br/>
        <input type="radio" value="recontato_radio" name="radio_button_clientes" id="recontato_radio" />
        <label for="recontato_radio">Recontato</label>
        <br/>
        <input type="radio" value="reagenda_dia_radio" name="radio_button_clientes" id="reagenda_dia_radio" />
        <label for="reagenda_dia_radio">Reagenda p/ dia</label>
        <br/>
        <input type="radio" value="reagenda_dia_rede" name="radio_button_clientes" id="reagenda_dia_rede" />
        <label for="reagenda_dia_rede">Reagenda p/ dia e rede</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="região_pais" />
        <label for="região_pais">Região país</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="região_pais_sintetico" />
        <label for="região_pais_sintetico">Região país - Sintético</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="ultimo_treinamento" />
        <label for="ultimo_treinamento">Ultimo Treinamento</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="primeira_compra" />
        <label for="primeira_compra" style="color: red">Primeira Compra</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="ultima_compra" />
        <label for="ultima_compra" style="color: red">Ultima Compra</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="ultimo_contato" />
        <label for="ultimo_contato" style="color: red">Último Contato</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="cobertura_rbs" />
        <label for="cobertura_rbs">Cobertura RBS</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="fora_cobertura_rbs" />
        <label for="fora_cobertura_rbs">Fora de cobertura RBS</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="outras_razoes" />
        <label for="outras_razoes">Outras Razões</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="visita_representantes" />
        <label for="visita_representantes">Visitas Representantes</label>
        <br/>
        
    </div>
    <br/>
    <div class="col-xs-6 thumbnail">
        
        <input type="radio" value="" name="radio_button_clientes" id="visitas_promotores"/>
        <label for="visitas_promotores">Visitas Promotores</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="contatos_alternativos" />
        <label for="contatos_alternativos">Contatos Alternativos</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="segmento_sintetico" />
        <label for="segmento_sintetico">Segmento - Sintético</label>
        <br/>
        
    </div>
    
    <div class="col-xs-6 thumbnail caption">
        
        <input type="radio" value="" name="radio_button_clientes" id="razao_sem_compras"/>
        <label for="razao_sem_compras">Razão</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="representante_sem_compras" />
        <label for="representante_sem_compras">Representante</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="rede_sem_compras" />
        <label for="rede_sem_compras">Rede</label>
        <br/>
        <input type="radio" value="" name="radio_button_clientes" id="rede_carteira_sem_compras" />
        <label for="rede_carteira_sem_compras">Rede/ Carteira</label>
        <br/>
        
    </div>
    
    <div class="col-xs-6 thumbnail caption">
        
        <div class="col-xs-12">
            <label for="empresa">Empresa: </label>
               <br/>
               <select value="" name="empresa"  class="col-xs-10" id="empresa">
                   <option value="">
                       
                   </option>
               </select>
        </div>
        
        <div class="col-xs-12">
                <br/>
                <input type="checkbox" indeterminate="indeterminate" name="visita_promotor" id="visita_promotor"  checkedvalue="S" uncheckedvalue="N" indeterminatevalue="R"/>
                <label for="visita_promotor">Visita Promotor</label>
                <br/>
                <input type="checkbox" indeterminate="indeterminate" name="ficha_contato" id="ficha_contato"  checkedvalue="S" uncheckedvalue="N" indeterminatevalue="R"/>
                <label for="ficha_contato">Ficha Contato</label>
                <br/>
                <input type="checkbox" indeterminate="indeterminate" name="personalizado" id="personalizado"  checkedvalue="S" uncheckedvalue="N" indeterminatevalue="R"/>
                <label for="personalizado">Presonalizado</label>
                <br/>
                <input type="checkbox" indeterminate="indeterminate" name="marca_propria" id="marca_propria"  checkedvalue="S" uncheckedvalue="N" indeterminatevalue="R"/>
                <label for="marca_propria">Marca Própria</label>
                <br/>
                <input type="checkbox" indeterminate="indeterminate" name="com_celular" id="com_celular"  checkedvalue="S" uncheckedvalue="N" indeterminatevalue="R"/>
                <label for="com_celular">Com Celular</label>
                <br/>
        </div>
       
     </div>
    <div>
        <button style="position:relative;margin: 15%" class="btn btn-success">Relatório</button>
    </div>
    
</div>
</form>
<?php echo form_close(); ?>

<script src="<?php echo base_url('public/js/jquery.min.js') ?>"></script>

<script>

$("select").each(function() {
    $(this).change(function(){
        atualiza_input();
    });
});

$("input").each(function() {
    $(this).change(function(){
        atualiza_select();
    });
});
 
 
function atualiza_input() {
    var inicial = $("#grupo_cadastro_inicial option:selected").val();
    $("#grupo_cadastro_inicial_input").val(inicial);
    var final = $("#grupo_cadastro_final option:selected").val();
    $("#grupo_cadastro_final_input").val(final);
    var inicial = $("#repres_inicial_select option:selected").val();
    $("#repres_inicial_input").val(inicial);
    var final = $("#repres_final_select option:selected").val();
    $("#repres_final_input").val(final);
    var inicial = $("#vend_inicial_select option:selected").val();
    $("#vend_inicial_input").val(inicial);
    var final = $("#vend_final_select option:selected").val();
    $("#vend_final_input").val(final);
    var inicial = $("#superv_inicial_select option:selected").val();
    $("#superv_inicial_input").val(inicial);
    var final = $("#superv_final_select option:selected").val();
    $("#superv_final_input").val(final);
    var promotor = $("#promotor_select option:selected").val();
    $("#promotor_input").val(promotor);
    var inicial = $("#rede_inicial_select option:selected").val();
    $("#rede_inicial_input").val(inicial);
    var final = $("#rede_final_select option:selected").val();
    $("#rede_final_input").val(final);
    var linha = $("#linha_varejo_select option:selected").val();
    $("#linha_varejo_input").val(linha);
}

function atualiza_select() {
    var inicial = $("#grupo_cadastro_inicial_input").val();
    $("#grupo_cadastro_inicial").val(inicial);
    var final = $("#grupo_cadastro_final_input").val();
    $("#grupo_cadastro_final").val(final);
    var inicial = $("#repres_inicial_input").val();
    $("#repres_inicial_select").val(inicial);
    var final = $("#repres_final_input").val();
    $("#repres_final_select").val(final);
    var inicial = $("#vend_inicial_input").val();
    $("#vend_inicial_select").val(inicial);
    var final = $("#vend_final_input").val();
    $("#vend_final_select").val(final);
    var inicial = $("#superv_inicial_input").val();
    $("#superv_inicial_select").val(inicial);
    var final = $("#superv_final_input").val();
    $("#superv_final_select").val(final);
    var promotor = $("#promotor_input").val();
    $("#promotor_select").val(promotor);
    var inicial = $("#rede_inicial_input").val();
    $("#rede_inicial_select").val(inicial);
    var final = $("#rede_final_input").val();
    $("#rede_final_select").val(final);
    var linha = $("#linha_varejo_input").val();
    $("#linha_varejo_select").val(linha);
}

</script>