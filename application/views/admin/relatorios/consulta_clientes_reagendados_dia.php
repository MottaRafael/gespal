<?php
        ini_set("memory_limit", "512M");
        ini_set("pcre.backtrack_limit", "5000000");
        
        $total_clientes = 0;
        
	$html = '<table width="100%" class="bpmTopic" style="overflow: hidden">';
           
            '<thead>';
            
            
                $resultado = array();
                foreach ($reagendado_dia as $reag_dia){
                    $resultado[$reag_dia->COD_CARTEIRA][] = $reag_dia;
                }
                
                foreach ($resultado as $chave => $valor){
                    $html .= '<tr class="headerrow">
                            <td align="left"><u>Data</u></td>
                            <td align="left"><u>Qtd. Clientes</u></td>
                    </tr>';
                    
                    $sql = $this->db->query('SELECT CARTEIRA FROM CARTEIRA WHERE COD_CARTEIRA = '.$chave);
                    $linha = $sql->row();
                    
                    $html .= '<tr>'
                            . '<td colspan="8" id="fantasia">Carteira:  <b>'.$chave." - " . $linha->CARTEIRA . '</b></td>'
                            . '</tr>';
                    
                    $html .= '</thead><tbody>';
                    $i=0;
                    foreach($valor as $c => $v){
                    $html .= '<tr>'
                        . '<td>'.date_format(new DateTime($v->DATA_RECONTATO), "d/m/Y").'</td>
                           <td style="white-space: nowrap; overflow: hidden;" align="left">'. $v->SOMA .'</td>'
                        . '</tr>';
                    
                    $total_clientes += $v->SOMA;
                    $i+=$v->SOMA;
                }
               
                $html .= '<tr class="separated">
                                  <td bgcolor="#b3b3b3">
                                          <b>#</b>
                                  </td>
                                  <td bgcolor="#b3b3b3" colspan="8">
                                          <b>TOTAL DO GRUPO : ' . number_format($i, 0, ',', '.') . '</b>
                                  </td>
                                  <td align="right" bgcolor="#b3b3b3">
                                  </td>
                 </tr>';
               
            }
                 
                $html .= '<tr>
                       <td bgcolor="#b3b3b3">
                               <b>#</b>
                       </td>
                       <td bgcolor="#b3b3b3" colspan="9">
                               <b>TOTAL GERAL DE CLIENTES</b>
                               <b>' . number_format($total_clientes, 0, ',', '.') . '</b>
                       </td>
               </tr>';
                     
               $html .= '</tbody></table>';
     
        
        $header =  ('<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-size: 7pt;">'
                 . '<tr>'
                 . '<td width="33%"></td>'
                 . '<td width="33%" align="center"><img src="' . base_url() . 'public/img/logo/logorel.png" height="4%" /></td>'
                 . '<td width="33%" style="text-align: right;"><br></td>'
                 . '</tr>'
                 . '</table>');
        
        $footer =  ('<div style="border-top: 1px solid #000000; font-size: 8pt; text-align: justify; padding-top: 3mm; ">'
                 . 'Este documento pode conter informação confidencial ou privilegiada, '
                 . 'sendo seu sigilo protegido. Se você não for o destinatário ou a pessoa '
                 . 'autorizada a receber este documento, não pode usar, copiar ou divulgar '
                 . 'as informações nele contidos ou tomar qualquer ação baseada nessas informações. '
                 . 'Se você recebeu este documento por engano, por favor, destrua-o imediatamente.<br>'
                 . '<div style="font-size: 9pt; text-align: center; padding-top: 3mm; ">Página {PAGENO} de {nb}</div>'
                 . '</div>');
        
        
        $mpdf = new Mpdf\Mpdf();
        $mpdf->SetHTMLHeader($header);
        $mpdf->SetHTMLFooter($footer);
        $stylesheet = file_get_contents(base_url('public/css/mpdfstyletables.css'));
	$mpdf->writeHTML($stylesheet, 1);
        $mpdf->AddPage('L', // L - landscape, P - portrait 
        '', '', '', '',
        5, // margin_left
        5, // margin right
       20, // margin top
       25, // margin bottom
        5, // margin header
        5); // margin footer
	$mpdf->WriteHTML($html, 2);
	$mpdf->Output();
        exit();
        
?>