<?php
        ini_set("memory_limit", "512M");
        ini_set("pcre.backtrack_limit", "5000000");
        
	$html = '<table width="100%" class="bpmTopic" style="overflow: hidden">
                <thead>
                    <tr class="headerrow">
                            <td align="left"><u>Cód.</u></td>
                            <td align="left" colspan="3"><u>Razão</u></td>
                            <td></td>
                            <td></td>
                            <td align="left" colspan="2"><u>Grupo Cad.</u></td>
                            <td></td>
                            <td align="left" colspan="2"><u>CNPJ</u></td>
                            <td></td>
                            <td align="left" colspan="2"><u>Cidade</u></td>
                            <td></td>
                            <td align="left"><u>UF</u></td>
                            <td align="left"><u>Bairro</u></td>
                            <td align="left"><u>Repres.</u></td>
                            <td align="left"><u>Vendedor</u></td>
                            <td align="right"><u>Grupo Rede</u></td>
                            <td align="left"><u>Recontato</u></td>
                    </tr>
                </thead>';
        
        $html .= '<tbody>';
        
                $total_clientes = 0;
                foreach ($razoes as $razao){
                    
                        $html .= '<tr>'
                            . '<td>'.$razao->CODIGO.'</td>
                            <td style="white-space: nowrap; overflow: hidden;" colspan="3">'.$razao->NOME_CLIENTE.'</td>
                            <td ></td>
                            <td ></td>
                            <td style="white-space: nowrap; overflow: hidden;" align="left" colspan="2">'.$razao->COD_GRUPO. ' - '. $razao->DESCRICAO .'</td>
                            <td></td>
                            <td style="white-space: nowrap; overflow: hidden;" align="left" colspan="2">'.$razao->CGC.'</td>
                            <td></td>
                            <td style="white-space: nowrap; overflow: hidden;" align="left" colspan="2">'.$razao->CIDADEFATURAMENTO.'</td>
                            <td></td>
                            <td style="white-space: nowrap; overflow: hidden;" align="left">'.$razao->UFFATURAMENTO.'</td>
                            <td style="white-space: nowrap; overflow: hidden;" align="left">'.$razao->BAIRROFATURAMENTO.'</td>
                            <td style="white-space: nowrap; overflow: hidden;" align="left">'.$razao->NOME_REPRES.'</td>
                            <td style="white-space: nowrap; overflow: hidden;" align="left">'.$razao->NOME_VEND.'</td>
                            <td style="white-space: nowrap; overflow: hidden;" align="right">'.$razao->COD_GRUPO_REDE.'</td>
                            <td style="white-space: nowrap; overflow: hidden;" align="right">'.date_format(new DateTime($razao->DATA_RECONTATO), "d/m/Y").'</td>'                        
                            . '</tr>';
                        $total_clientes++;
                        
                     }
                     
                $html .= '<tr>
                       <td bgcolor="#b3b3b3">
                               <b>#</b>
                       </td>
                       <td bgcolor="#b3b3b3" colspan="10">
                               <b>TOTAL GERAL DE CLIENTES</b>
                       </td>
                       <td align="right" bgcolor="#b3b3b3"></td>
                       <td align="right" bgcolor="#b3b3b3"></td>
                       <td align="right" bgcolor="#b3b3b3"></td>
                       <td align="right" bgcolor="#b3b3b3"></td>
                       <td align="right" bgcolor="#b3b3b3"></td>
                       <td align="right" bgcolor="#b3b3b3"></td>
                       <td align="right" bgcolor="#b3b3b3"></td>
                       <td align="right" bgcolor="#b3b3b3"></td>
                       <td align="right" bgcolor="#b3b3b3"></td>
                       <td align="right" bgcolor="#b3b3b3">
                               <b>' . number_format($total_clientes, 0, ',', '.') . '</b>
                       </td>
               </tr>';
                     
               $html .= '</tbody></table>';
     
        
        $header =  ('<table width="100%" style="border-bottom: 1px solid #000000; vertical-align: bottom; font-size: 7pt;">'
                 . '<tr>'
                 . '<td width="33%"></td>'
                 . '<td width="33%" align="center"><img src="' . base_url() . 'public/img/logo/logorel.png" height="4%" /></td>'
                 . '<td width="33%" style="text-align: right;"><br></td>'
                 . '</tr>'
                 . '</table>');
        
        $footer =  ('<div style="border-top: 1px solid #000000; font-size: 8pt; text-align: justify; padding-top: 3mm; ">'
                 . 'Este documento pode conter informação confidencial ou privilegiada, '
                 . 'sendo seu sigilo protegido. Se você não for o destinatário ou a pessoa '
                 . 'autorizada a receber este documento, não pode usar, copiar ou divulgar '
                 . 'as informações nele contidos ou tomar qualquer ação baseada nessas informações. '
                 . 'Se você recebeu este documento por engano, por favor, destrua-o imediatamente.<br>'
                 . '<div style="font-size: 9pt; text-align: center; padding-top: 3mm; ">Página {PAGENO} de {nb}</div>'
                 . '</div>');
        
        
        $mpdf = new Mpdf\Mpdf();
        $mpdf->SetHTMLHeader($header);
        $mpdf->SetHTMLFooter($footer);
        $stylesheet = file_get_contents(base_url('public/css/mpdfstyletables.css'));
	$mpdf->writeHTML($stylesheet, 1);
        $mpdf->AddPage('L', //L - landscape, P - portrait
        '', '', '', '',
        5, // margin_left
        5, // margin right
       20, // margin top
       25, // margin bottom
        5, // margin header
        5); // margin footer
	$mpdf->WriteHTML($html, 2);
	$mpdf->Output();
        exit();
        
?>