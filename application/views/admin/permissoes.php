<script src="<?php echo base_url('public/js/jquery.min.js') ?>"></script>
<section class="content-header">
	<h1>Controle de Permissões
		<small></small>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-table"></i> Início</a></li>
		<li><a href="#">Gerencial</a></li>
		<li class="active">Permissões</li>
	</ol>
</section>

<style>
	#scroll {
	  height:calc(100vh - 240px);
	  overflow:auto;
	  overflow-x: hidden;
	}
</style>
<script>
	$(function () {
            $("#tableListaUsuariosPermissoes").DataTable({
                    "language": {
                        "lengthMenu": "Mostrando _MENU_ registros por página",
                        "zeroRecords": "Nada encontrado",
                        "info": "Mostrando página _PAGE_ de _PAGES_",
                        "infoEmpty": "Nenhum registro disponível",
                        "infoFiltered": "(filtrado de _MAX_ registros no total)",
                        "search": "Buscar:",
                        "paginate": {
                                "previous": "Anterior",
                                "next": "Próxima"
                        }
                    },
                    "lengthChange": true,
                    "searching": true,
                    "ordering": false,
                    "info": true,
                    "autoWidth": true
            });
	});
</script>

<section class="content">
    <div class="row">
        <div class="col-xs-12">	
            <div class="box box-default">
                <div class="panel panel-default col-lg-12">
                    <div class="row">
                        <div class="col-lg-12" >
                            <div class="box-header with-border" >
                                <h3 class="box-title">Usuários</h3>

                            </div>
                            <div class="box-body with-border" id="scroll">

                                <table class="table table-striped table-bordered table-hover" id="tableListaUsuariosPermissoes" name="tableListaUsuariosPermissoes">
                                    <thead>
                                        <tr>
                                            <th>Usuário</th>
                                            <th>Ramal</th>
                                            <th>Ativo</th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <?php
//                                        if ($usuarios = base_url('usuarios/usuariosAtivos')) {
                                            $tabela = '';
                                            foreach ($usuarios as $result) {
                                                if (trim($result->ATIVO) == 'N') {
                                                    $badge = ' <span class="label label-danger label-as-badge">Desativado</span>';
                                                } else {
                                                    $badge = '';
                                                }
                                                $tabela = $tabela . '<tr class="odd gradeX">' .
                                                        '<td class="col-md-10" style="cursor:pointer" onclick="abrirPermissoes('.trim("'$result->COD_USUARIO'").')">' . $badge . ' ' . $result->NOME . '</td>' .
                                                        '<td class="col-md-10" style="cursor:pointer" onclick="abrirPermissoes('.trim("'$result->COD_USUARIO'").')">' . $badge . ' ' . $result->RAMAL . '</td>' .
                                                        '<td class="col-md-10" style="cursor:pointer" onclick="abrirPermissoes('.trim("'$result->COD_USUARIO'").')">' . $badge . ' ' . $result->ATIVO . '</td>' .
                                                        '</tr>';
                                            }

                                            '</table>';
                                            echo $tabela;
//                                        } else {
//                                            echo 'Nenhuma usuário encontrado';
//                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
function abrirPermissoes(arquivo){
    window.location.href = "<?php echo base_url('admin/permissoes/alterar/') ?>"+arquivo;
}


</script>