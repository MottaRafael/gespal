<?php 
if($titulo === "Usuários Ativos"){
    $inativos = anchor(base_url('admin/usuariosinativos'), '<i class="fa fa-filter" aria-hidden="true"></i> Ir para Inativos');
    echo $inativos . "<hr/>";
}
?>
<table id="tabelaUsuarios" class="display" style="width:100%">
    <thead>
        <tr>
            <td>
                Nome
            </td>
            <td>
                Ramal
            </td>
            <td>
                Alterar
            </td>
        </tr>
    </thead>
    <tbody>
        
    </tbody>
</table>

