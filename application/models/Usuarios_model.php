<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model {
    
    public $cod_usuario;
    public $nome;
    public $senha;

   public function __construct() {
            parent::__construct();
   }
   
   public function retorna_dados_usuario($cod_usuario){
        $this->db->select('NOME, COD_USUARIO');
        $this->db->from('USUARIOS');
        $this->db->where('COD_USUARIO', $cod_usuario);
        return $this->db->get()->result();
   }
   
   public function alterar($id){
        $this->db->select('NOME_COMPLETO, COD_USUARIO, RAMAL, NOME');
        $this->db->from('USUARIOS');
        $this->db->where('COD_USUARIO ', $id);
        return $this->db->get()->result();
   }
   
   public function permissoes($id){
        $this->db->select('NOME_COMPLETO, COD_USUARIO, RAMAL, NOME');
        $this->db->from('USUARIOS');
        $this->db->where('COD_USUARIO ', $id);
        return $this->db->get()->result();
   }
   
   public function permissoesAdicionar($idUser, $idMenu){
       $dados['COD_USUARIO'] = $idUser;
       $dados['COD_MENU'] = $idMenu;
       $dados['PODE_ACESSAR'] = 'S';
       return $this->db->insert('USUARIOS_PERMISSOES_MENUS', $dados);
   }
   
   public function permissoesExcluir($idUser, $idMenu){
       $this->db->where('COD_USUARIO', $idUser);
       $this->db->where('COD_MENU', $idMenu);
       return $this->db->delete('USUARIOS_PERMISSOES_MENUS');
   }
   
}
