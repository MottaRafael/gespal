<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_model extends CI_Model {

   public function __construct() {
            parent::__construct();
   }
   
   public function inserir_menu($nome_do_menu, $icone, $caminho_menu, $descricao, $indice)
    {
        $dados['NOME'] = $nome_do_menu;
        $dados['ICONE'] = $icone;
        $dados['NOME_ARQUIVO'] = $caminho_menu;
        $dados['DESCRICAO'] = $descricao;
        $dados['INDICE'] = $indice;
        
        return $this->db->insert('ITENS_MENU', $dados);
    }
   
}
